/*
 * Copyright 2019 Lars Niedziolka
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React from 'react';
import Chart from 'react-apexcharts';
import { MEMORY_SUBSCRIPTION } from './GraphQLQuery';
import { options } from './MemoryChartOptions';

/**
 * A area chart diagram with with updating used, free and total memory.
 */
export class MemoryChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            now: Date.now(),
            useds: [],
            frees: [],
            options: options(this.props.chartSize, () => this.state.now),
            series: [
                { name: 'used memory', data: [] },
                { name: 'free memory', data: [], }
            ]
        };
    }

    render() {
        return (
            <div className="container memorychart">
                <Chart type="area" options={this.state.options} series={this.state.series} />
            </div>
        );
    }
    componentDidMount() {
        this.subscription = this.props.client
            .subscribe({ query: MEMORY_SUBSCRIPTION, fetchPolicy: "network-only" })
            .subscribe(
                data => this.updateMemoryChart(data.data.memory),
                error => console.error(error),
            );
    }
    componentWillUnmount() {
        this.subscription.dispose();
    }
    updateMemoryChart(memory) {
        const now = Date.now();
        if (this.state.useds.length >= 10 * this.props.chartSize) {
            this.state.useds.shift();
            this.state.frees.shift();
        }
        this.state.useds.push([now, memory.used]);
        this.state.frees.push([now, memory.free]);
        this.setState({
            now: now,
            series: [
                { data: this.state.useds.slice() },
                { data: this.state.frees.slice() }
            ]
        });
    }
}
