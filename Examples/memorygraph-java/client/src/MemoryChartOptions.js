/*
 * Copyright 2019 Lars Niedziolka
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const SEC = 1000; // ms

/**
 * Options for MemoryChart.
 * 
 * @param {number} chartSize number of data points to show 
 * @param {*} nowProvider function to provide the now time
 */
export function options(chartSize, nowProvider) {
    return {
        chart: {
            id: 'memory',
            stacked: true,
            animations: {
                enabled: true,
                easing: 'linear',
                dynamicAnimation: {
                    speed: 2000
                }
            },
            zoom: {
                type: 'x',
                enabled: true
            },
            toolbar: {
                autoSelected: 'zoom'
            }
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'smooth'
        },
        colors: ['#77B6EA', '#545454'],
        fill: {
            type: 'gradient',
            gradient: {
                shadeIntensity: 1,
                inverseColors: false,
                opacityFrom: 0.5,
                opacityTo: 0,
                stops: [0, 90, 100]
            },
        },
        title: {
            text: 'Memory',
            align: 'left'
        },
        xaxis: {
            type: 'datetime',
            range: chartSize * SEC,
            tickAmount: chartSize / 10,
            labels: { formatter: value => `-${((nowProvider() - value) / SEC).toFixed(0)}s` }
        },
        yaxis: {
            min: 0,
            forceNiceScale: true,
            labels: { formatter: value => `${value.toFixed(3)} MB` }
        },
        legend: {
            position: 'top',
            horizontalAlign: 'center',
            floating: true,
            offsetY: -30,
            offsetX: -5
        }
    };
}
