/*
 * Copyright 2019 Lars Niedziolka
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React from 'react';
import { MEMORY_REDUCE, MEMORY_REQUEST } from './GraphQLQuery';

/**
 * A table with columns used, free and total memory.
 * Provides two actions do query the current values or
 * to reduce the memory and query the current values.
 */
export class MemoryTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = { used: 1.2, free: 2.3, total: 3.5 };
    }
    componentDidMount() {
        this.graphqlQuery(this.props.client);
    }
    render() {
        return (<div className="container memorytable">
            <div className="row">
                <div className="col-3">
                    <button onClick={() => this.graphqlQuery(this.props.client)}>
                        Query Memory
          </button>
                </div>
                <div className="col-3 key">Used</div>
                <div className="col-3 key">Free</div>
                <div className="col-3 key">Total</div>
            </div>
            <div className="row">
                <div className="col-3">
                    <button onClick={() => this.graphqlMutation(this.props.client)}>
                        Reduce Memory
          </button>
                </div>
                <div className="col-3 value">{this.state.used} MB</div>
                <div className="col-3 value">{this.state.free} MB</div>
                <div className="col-3 value">{this.state.total} MB</div>
            </div>
        </div>);
    }
    graphqlQuery(client) {
        client
            .query({ query: MEMORY_REQUEST, fetchPolicy: "network-only" })
            .then(data => this.updateMemoryTable(data.data.memory))
            .catch(error => console.error(error));
    }
    graphqlMutation(client) {
        client
            .mutate({ mutation: MEMORY_REDUCE })
            .then(data => this.updateMemoryTable(data.data.memory))
            .catch(error => console.error(error));
    }
    updateMemoryTable(memory) {
        console.log(memory);
        this.setState(() => memory);
    }
}
