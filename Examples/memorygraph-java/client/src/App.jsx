/*
 * Copyright 2019 Lars Niedziolka
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React from 'react';
import { ApolloConsumer, ApolloProvider } from 'react-apollo';
import './App.css';
import { client } from './GraphQLQuery';
import { MemoryChart } from './MemoryChart';
import { MemoryTable } from './MemoryTable';

function App() {
  return (
    <ApolloProvider client={client}>
      <div className="container">
        <h1>GraphQL Query / Mutation over http</h1>
        <ApolloConsumer>
          {client => <MemoryTable client={client} />}
        </ApolloConsumer>
        <h1>GraphQL Subscription over websocket</h1>
        <MemoryChart chartSize={60} client={client} />
      </div>
    </ApolloProvider>
  );
}

export default App;
