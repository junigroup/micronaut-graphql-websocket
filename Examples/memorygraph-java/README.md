# Micronaut GraphQL WebSocket with Web UI

Start the application:

	./gradlew clean run

Open Playground in a web browser:

	http://localhost:8080

The web client is programmed as [Create React App](https://github.com/facebook/create-react-app) and is located in subfolder `client`.
To develop the client code [Yarn](https://yarnpkg.com/lang/en/) needs to be installed.
