/*
 * Copyright 2019 original author
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package example;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import org.reactivestreams.Publisher;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import io.reactivex.Flowable;

/**
 * {@link DataFetcher} as method references.
 * 
 * @author Lars Niedziolka
 */
@Singleton
public class MemoryService {
	private static final float MB = 1024 * 1024;

	private final Runtime runtime = Runtime.getRuntime();

	/** One-time request */
	public Float queryMemory(DataFetchingEnvironment environment) {
		return getUsedMemory();
	}

	/** Manipulation + result */
	public Float reduceMemory(DataFetchingEnvironment environment) {
		runtime.gc();
		return getUsedMemory();
	}

	/** Subscribing request */
	public Publisher<Float> subscribeMemory(DataFetchingEnvironment environment) {
		int interval = Math.max(1, environment.getArgument("interval"));
		return Flowable.interval(0, interval, TimeUnit.SECONDS).map(timestamp -> getUsedMemory());
	}

	private float getUsedMemory() {
		long totalMemory = runtime.totalMemory();
		long freeMemory = runtime.freeMemory();
		long usedMemory = totalMemory - freeMemory;
		return usedMemory / MB;
	}

}
