/*
 * Copyright 2019 original author
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package example;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Optional;

import javax.inject.Singleton;

import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Factory;
import io.micronaut.core.io.ResourceResolver;

/**
 * @author Lars Niedziolka
 */
@Factory
public class GraphQLFactory {

	@Bean
	@Singleton
	GraphQL createGraphQL(ResourceResolver resourceResolver, MemoryService memoryService) {
		Optional<InputStream> schemaResource = resourceResolver.getResourceAsStream("classpath:schema.graphqls");

		// Parse the schema.
		TypeDefinitionRegistry typeRegistry = new TypeDefinitionRegistry();
		SchemaParser schemaParser = new SchemaParser();
		typeRegistry.merge(schemaParser.parse(new BufferedReader(new InputStreamReader(schemaResource.get()))));

		// Create the runtime wiring.
		RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring() //
				.type("Query", typeWiring -> typeWiring.dataFetcher("memory", memoryService::queryMemory)) //
				.type("Mutation", typeWiring -> typeWiring.dataFetcher("memory", memoryService::reduceMemory)) //
				.type("Subscription", typeWiring -> typeWiring.dataFetcher("memory", memoryService::subscribeMemory)) //
				.build();

		// Create the executable schema.
		SchemaGenerator schemaGenerator = new SchemaGenerator();
		GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeRegistry, runtimeWiring);

		// Return the GraphQL bean.
		return GraphQL.newGraphQL(graphQLSchema).build();
	}

}
