# Micronaut GraphQL WebSocket with [Prisma Playground](https://github.com/prisma/graphql-playground)

Start the application:

	./gradlew clean run

Open Playground in a web browser:

	http://localhost:8080

Try following Query in Playground:

	query AsQuery {
	  memory
	}
	
	mutation AsMutation {
	  memory
	}
	
	subscription AsSubscription {
	  memory
	}
	
	subscription LazySubscription {
	  memory(60)
	}
