# Open Points #
* Apollo Protocol &#10004;
* Unit Test &#10004;
* Embedded Server Tests &#10004;
* Query over WebSocket &#10004;
* Mutation over WebSocket &#10004;
* Keep Alive Signal &#10004;

* Example: Playground &#10004;
* Example: apollo JS &#10004;
* Example: reactive webserver

* Configuration &#10004;
* Documentation
