/*
 * Copyright 2019 original author
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.junigroup.micronaut.graphql.websocket;

import io.micronaut.http.HttpHeaders;
import io.micronaut.websocket.WebSocketSession;

/**
 * The implementation of this interface should handle a WebSocket communication.
 * <p>
 * The {@link GraphQLWebSocket} service chooses the implementation based on the
 * {@link HttpHeaders#SEC_WEBSOCKET_PROTOCOL WebSocket protocol}.
 * 
 * @author Lars Niedziolka
 */
public interface SubscriptionProtocolHandler {

	/**
	 * Handle the next {@link WebSocketSession WebSocket} text message.
	 * <p>
	 * Expect the message as JSON, the interpretation depends on the implementing protocol.
	 * 
	 * @param session the WebSocket session to the client
	 * @param message the text message
	 */
	void onMessage(WebSocketSession session, String message);

	/**
	 * Server is closing the session or client has closed the session.
	 * <p>
	 * Stop all open subscriptions.
	 * 
	 * @param session the closing WebSocket session
	 */
	void shutdown(WebSocketSession session);

}
