/*
 * Copyright 2017-2019 original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.junigroup.micronaut.graphql.websocket;

import static io.micronaut.configuration.graphql.GraphQLConfiguration.PREFIX;

import java.time.Duration;

import io.micronaut.context.annotation.ConfigurationProperties;
import io.micronaut.core.util.Toggleable;

/**
 * Configuration properties for GraphQL over WebSocket.
 *
 * @author Lars Niedziolka
 */
@ConfigurationProperties(PREFIX)
public class GraphQLWebSocketConfiguration implements Toggleable {

	/** The prefix to use for all GraphQL WebSocket configuration properties. */
	public static final String PREFIX = "graphql-ws";

	/** The configuration name for {@link #isEnabled()}. */
	public static final String ENABLED = PREFIX + ".enabled";

	/** The default configuration value for {@link #isEnabled()}. */
	public static final boolean DEFAULT_ENABLED = true;

	/** The configuration name for {@link #getPath()}. */
	public static final String PATH = PREFIX + ".path";

	/** The default value for {@link #getPath()} */
	public static final String DEFAULT_PATH = "/subscriptions";

	/** The configuration name for {@link #getKeepAlive()}. */
	public static final String KEEP_ALIVE = PREFIX + ".keep-alive";

	/** The default value for {@link #getKeepAlive()}: 15 seconds */
	public static final String DEFAULT_KEEP_ALIVE = "PT15S";

	/** Current configuration value for {@link #isEnabled()}. */
	protected boolean enabled = DEFAULT_ENABLED;

	/** Current configuration value for {@link #getPath()} */
	protected String path = DEFAULT_PATH;

	/** The configuration string for {@link #getKeepAlive()} */
	static final String KEEP_ALIVE_CONFIG = "${" + KEEP_ALIVE + ":" + DEFAULT_KEEP_ALIVE + "}";

	/** Current configuration value for {@link #getKeepAlive()} */
	protected Duration keepAlive = Duration.parse(DEFAULT_KEEP_ALIVE);

	/**
	 * Gets the configuration whether GraphQL over WebScoket is enabled.
	 *
	 * @return whether GraphQL over WebSocket is enabled
	 */
	@Override
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Gets the configuration value of the GraphQL over WebSocket path.
	 *
	 * @return the GraphQL over WebSocket path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Gets the period of the keep alive timer in milliseconds.
	 * <p>
	 * The keep alive timer is disabled if the value is zero.
	 * 
	 * @return the keep alive timer period in milliseconds.
	 */
	public Duration getKeepAlive() {
		return keepAlive;
	}
}
