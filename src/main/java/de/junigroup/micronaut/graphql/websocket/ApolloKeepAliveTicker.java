/*
 * Copyright 2019 original author
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.junigroup.micronaut.graphql.websocket;

import java.time.Duration;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.atomic.AtomicInteger;

import javax.inject.Named;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.micronaut.configuration.graphql.GraphQLJsonSerializer;
import io.micronaut.context.annotation.Value;
import io.micronaut.core.util.Toggleable;
import io.micronaut.http.MediaType;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.TaskScheduler;
import io.micronaut.websocket.WebSocketBroadcaster;
import io.micronaut.websocket.WebSocketSession;

/**
 * The implementation of the {@link ApolloKeepAliveHandler}.
 * 
 * @author Lars Niedziolka
 */
@Singleton
class ApolloKeepAliveTicker implements ApolloKeepAliveHandler, Toggleable {
	private static final String KEEP_ALIVE_KEY = "KEEP_ALIVE";
	private static final Boolean KEEP_ALIVE_VALUE = true;

	private static final Logger log = LoggerFactory.getLogger(ApolloKeepAliveHandler.class);
	private final String keepAliveMessage;
	private final WebSocketBroadcaster broadcaster;
	private final TaskScheduler scheduler;
	private final AtomicInteger registerCounter = new AtomicInteger(0);
	private final Duration period;
	private final boolean disabled;
	private ScheduledFuture<?> subscription;

	public ApolloKeepAliveTicker(GraphQLJsonSerializer graphQLJsonSerializer, WebSocketBroadcaster broadcaster,
			@Named(TaskExecutors.SCHEDULED) TaskScheduler scheduler, @Value(GraphQLWebSocketConfiguration.KEEP_ALIVE_CONFIG) Duration period) {
		this.keepAliveMessage = createKeepAliveMessage(graphQLJsonSerializer);
		this.broadcaster = broadcaster;
		this.scheduler = scheduler;
		this.period = period;
		this.disabled = period.isZero() || period.isNegative();
	}

	private String createKeepAliveMessage(GraphQLJsonSerializer graphQLJsonSerializer) {
		ApolloOperationMessage operationMessage = new ApolloOperationMessage(ApolloOperationMessage.Type.GQL_KEEP_ALIVE, null, null);
		String keepAliveMessage = graphQLJsonSerializer.serialize(operationMessage);
		return keepAliveMessage;
	}

	@Override
	public boolean isEnabled() {
		return !disabled;
	}

	// visible for testing
	Duration getPeriod() {
		return period;
	}

	@Override
	public void register(WebSocketSession session) {
		if (disabled) {
			return;
		}

		log.debug("Keep alive {} registered", session.getId());
		firstKeepAlive(session);
		if (!hasKeepAliveMarker(session)) {
			session.put(KEEP_ALIVE_KEY, KEEP_ALIVE_VALUE);
			int counterBefore = registerCounter.getAndIncrement();
			if (counterBefore == 0) {
				startKeepAliveBroadcast();
			}
		}

	}

	@Override
	public void deregister(WebSocketSession session) {
		if (disabled) {
			return;
		}

		log.debug("Keep alive {} deregistered", session.getId());
		if (hasKeepAliveMarker(session)) {
			session.remove(KEEP_ALIVE_KEY);
			int counterAfter = registerCounter.decrementAndGet();
			if (counterAfter == 0) {
				stopKeepAliveBroadcast();
			}
		}
	}

	private void startKeepAliveBroadcast() {
		synchronized (this) {
			subscription = scheduler.scheduleAtFixedRate(period, period, this::broadcastKeepAlive);
		}
	}

	private void stopKeepAliveBroadcast() {
		synchronized (this) {
			subscription.cancel(false);
			subscription = null;
		}
	}

	private void firstKeepAlive(WebSocketSession session) {
		session.sendSync(keepAliveMessage, MediaType.APPLICATION_JSON_TYPE);
	}

	private void broadcastKeepAlive() {
		log.trace("Keep alive broadcast");
		broadcaster.broadcastSync(keepAliveMessage, MediaType.APPLICATION_JSON_TYPE, ApolloKeepAliveTicker::hasKeepAliveMarker);
	}

	private static boolean hasKeepAliveMarker(WebSocketSession session) {
		Boolean hasKeepAliveMarker = session.get(KEEP_ALIVE_KEY, Boolean.class, Boolean.FALSE);
		return hasKeepAliveMarker;
	}

}
