/*
 * Copyright 2019 original author
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.junigroup.micronaut.graphql.websocket;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import graphql.ExecutionResult;
import graphql.GraphqlErrorBuilder;
import io.micronaut.configuration.graphql.GraphQLExecutionResultHandler;
import io.micronaut.configuration.graphql.GraphQLInvocation;
import io.micronaut.configuration.graphql.GraphQLInvocationData;
import io.micronaut.configuration.graphql.GraphQLJsonSerializer;
import io.micronaut.configuration.graphql.GraphQLResponseBody;
import io.micronaut.context.annotation.Prototype;
import io.micronaut.core.async.publisher.Publishers;
import io.micronaut.http.MediaType;
import io.micronaut.websocket.CloseReason;
import io.micronaut.websocket.WebSocketSession;
import io.reactivex.Flowable;

/**
 * Implements the apollo protocol for GraphQL subscription over WebSocket:
 * <a href="https://github.com/apollographql/subscriptions-transport-ws/blob/master/PROTOCOL.md">PROTOCOL.md</a>
 * <p>
 * 
 * @author Lars Niedziolka
 */
@Prototype
public class ApolloSubscriptionProtocolHandler implements SubscriptionProtocolHandler {
	private static final CloseReason TERMINATE_CLOSE_REASON = CloseReason.NORMAL;
	private static final CloseReason SHUTDOWN_CLOSE_REASON = CloseReason.GOING_AWAY;

	private static final boolean REQUIRED_ATTIBUTE = true;
	private static final boolean OPTIONAL_ATTRIBUTE = false;

	private static final String NO_ID = null;
	private static final Object NO_PAYLOAD = null;

	private static final Class<Map<String, Object>> JSON_MAP = uncheckedCast(Map.class);

	private static final Logger log = LoggerFactory.getLogger(ApolloSubscriptionProtocolHandler.class);

	private final GraphQLInvocation graphQLInvocation;
	private final GraphQLExecutionResultHandler graphQLExecutionResultHandler;
	private final GraphQLJsonSerializer graphQLJsonSerializer;
	private final ApolloKeepAliveHandler keepAliveHandler;

	/**
	 * Default constructor.
	 *
	 * @param graphQLInvocation the {@link GraphQLInvocation} instance
	 * @param graphQLExecutionResultHandler the {@link GraphQLExecutionResultHandler} instance
	 * @param graphQLJsonSerializer the {@link GraphQLJsonSerializer} instance
	 * @param keepAliveHandler the {@link ApolloKeepAliveHandler} instance
	 */
	public ApolloSubscriptionProtocolHandler(GraphQLInvocation graphQLInvocation, GraphQLExecutionResultHandler graphQLExecutionResultHandler,
			GraphQLJsonSerializer graphQLJsonSerializer, ApolloKeepAliveHandler keepAliveHandler) {
		this.graphQLInvocation = graphQLInvocation;
		this.graphQLExecutionResultHandler = graphQLExecutionResultHandler;
		this.graphQLJsonSerializer = graphQLJsonSerializer;
		this.keepAliveHandler = keepAliveHandler;
	}

	/**
	 * Handle the next {@link WebSocketSession WebSocket} text message.
	 * <p>
	 * Expect the message as JSON of {@link ApolloOperationMessage}.
	 * 
	 * @param session the WebSocket session to the client
	 * @param message the text message
	 */
	@Override
	public void onMessage(WebSocketSession session, String message) {
		ApolloOperationMessage operationMessage;
		try {
			operationMessage = graphQLJsonSerializer.deserialize(message, ApolloOperationMessage.class);
		} catch (RuntimeException e) {
			log.warn("Error parsing message", e);
			sendMessage(session, ApolloOperationMessage.Type.GQL_CONNECTION_ERROR, NO_ID, NO_PAYLOAD);
			return;
		}

		switch (operationMessage.getType()) {
		case GQL_CONNECTION_INIT:
			initConnection(session, operationMessage);
			break;
		case GQL_START:
			startSubscription(session, operationMessage);
			break;
		case GQL_STOP:
			stopSubscription(session, operationMessage);
			break;
		case GQL_CONNECTION_TERMINATE:
			terminateConnection(session);
			break;
		default:
			log.warn("Invalid operation message type: ", operationMessage.getType());
			sendMessage(session, ApolloOperationMessage.Type.GQL_CONNECTION_ERROR, NO_ID, NO_PAYLOAD);
		}
	}

	private void initConnection(WebSocketSession session, ApolloOperationMessage operationMessage) {
		log.debug("Connection_init: {}", session.getId());
		sendMessage(session, ApolloOperationMessage.Type.GQL_CONNECTION_ACK, NO_ID, NO_PAYLOAD);
		keepAliveHandler.register(session);
	}

	private void terminateConnection(WebSocketSession session) {
		try {
			log.debug("Connection_terminate: {}", session.getId());
			stopAllSubscriptions(session);
			session.close(TERMINATE_CLOSE_REASON);
		} catch (Exception e) {
			log.error("Error closing websocket session!", e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void shutdown(WebSocketSession session) {
		try {
			log.debug("Shutdown: {}", session.getId());
			stopAllSubscriptions(session);
			if (session.isOpen()) {
				session.close(SHUTDOWN_CLOSE_REASON);
			}
		} catch (Exception e) {
			log.error("Error closing websocket session!", e);
		}
	}

	private final Object syncLock = new Object();
	private final Map<String, Subscription> openSubscriptions = new ConcurrentHashMap<>();

	private void startSubscription(WebSocketSession session, ApolloOperationMessage operationMessage) {
		log.debug("Start: {} {}", session.getId(), operationMessage.getId());

		new GraphQLSubscription(session, operationMessage).execute();
	}

	private void stopSubscription(WebSocketSession session, ApolloOperationMessage operationMessage) {
		log.debug("Stop: {} {}", session.getId(), operationMessage.getId());
		Subscription subscription = removeSubscription(operationMessage.getId());
		if (subscription != null) {
			log.debug("Subscription {} canceled", operationMessage.getId());
			subscription.cancel();
		}
	}

	private void addSubscription(String operationMessageId, Subscription subscription) {
		synchronized (syncLock) {
			openSubscriptions.put(operationMessageId, subscription);
		}
	}

	private Subscription removeSubscription(String operationMessageId) {
		Subscription subscription;
		synchronized (syncLock) {
			subscription = openSubscriptions.remove(operationMessageId);
		}
		return subscription;
	}

	private void stopAllSubscriptions(WebSocketSession session) {
		log.debug("Stop_all: {}", session.getId());

		ArrayList<Entry<String, Subscription>> subscriptions;
		synchronized (syncLock) {
			subscriptions = new ArrayList<>(openSubscriptions.entrySet());
			openSubscriptions.clear();
		}

		keepAliveHandler.deregister(session);
		subscriptions.forEach(entry -> {
			String operationMessageId = entry.getKey();
			Subscription subscription = entry.getValue();
			try {
				log.debug("Subscription {} canceled", operationMessageId);
				subscription.cancel();
			} catch (RuntimeException e) {
				log.error("Cancel subscription {} failed: {}", operationMessageId, e.getMessage(), e);
			}
		});
	}

	private void sendMessage(WebSocketSession session, ApolloOperationMessage.Type type, String id, Object payload) {
		ApolloOperationMessage apolloOperationMessage = new ApolloOperationMessage(type, id, payload);
		String message = graphQLJsonSerializer.serialize(apolloOperationMessage);
		session.sendSync(message, MediaType.APPLICATION_JSON_TYPE);
	}

	private class GraphQLSubscription {
		private final WebSocketSession session;
		private final String id;
		private final GraphQLInvocationData invocationData;

		public GraphQLSubscription(WebSocketSession session, ApolloOperationMessage operationMessage) {
			this.session = session;
			this.id = operationMessage.getId();
			this.invocationData = convertPayloadToInvocationData2(operationMessage.getPayload());
		}

		private GraphQLInvocationData convertPayloadToInvocationData2(Object payload) {
			Map<String, Object> requestMap = expect(JSON_MAP, payload, "payload", REQUIRED_ATTIBUTE);
			String query = expect(String.class, requestMap.get("query"), "payload.query", REQUIRED_ATTIBUTE);
			String operationName = expect(String.class, requestMap.get("operationName"), "payload.operationName", OPTIONAL_ATTRIBUTE);
			Map<String, Object> variables = expect(JSON_MAP, requestMap.get("variables"), "payload.variables", OPTIONAL_ATTRIBUTE);
			GraphQLInvocationData invocationData = new GraphQLInvocationData(query, operationName, variables);
			return invocationData;
		}

		public void execute() {
			Flowable<ExecutionResult> graphQLSubscription = Flowable.fromPublisher(graphQLInvocation.invoke(invocationData, null)) //
					.flatMap(subscriptionExecutionResult -> lift(subscriptionExecutionResult));
			Flowable.fromPublisher(graphQLExecutionResultHandler.handleExecutionResult(graphQLSubscription)) //
					.subscribe(this::sendData, this::onError, this::onComplete, this::onSubscribe);
		}

		private void onError(Throwable error) {
			log.debug("Subscription {} error: {}", id, error.getMessage());
			Map<String, Object> errorBody = GraphqlErrorBuilder.newError() //
					.message(error.getMessage()) //
					.build().toSpecification();
			sendMessage(session, ApolloOperationMessage.Type.GQL_ERROR, id, errorBody);
		}

		private void sendData(GraphQLResponseBody next) {
			log.debug("Subscription {} data: {}", id, next.getSpecification());
			sendMessage(session, ApolloOperationMessage.Type.GQL_DATA, id, next);
		}

		private void onSubscribe(Subscription subscription) {
			addSubscription(id, subscription);
			subscription.request(Long.MAX_VALUE);
		}

		private Publisher<ExecutionResult> lift(ExecutionResult subscriptionExecutionResult) {
			if (subscriptionExecutionResult.getData() instanceof Publisher) {
				log.debug("Subscription {} started", id);
				return subscriptionExecutionResult.getData();
			} else {
				log.debug("Subscription {} abort {}", id, subscriptionExecutionResult.getErrors());
				return Publishers.just(subscriptionExecutionResult);
			}
		}

		private void onComplete() {
			log.debug("Subscription {} complete", id);
			sendMessage(session, ApolloOperationMessage.Type.GQL_COMPLETE, id, NO_PAYLOAD);
			removeSubscription(id);
		}

	}

	/**
	 * Cast the given object to the expected but required type.
	 * 
	 * @param <T> the required generic type
	 * @param requiredType the required type
	 * @param object the general object
	 * @param path a path like description of the casted object
	 * @param expectNonNull <code>true</code> if object has to be non-null
	 * @return the object
	 */
	private <T> T expect(Class<T> requiredType, Object object, String path, boolean expectNonNull) {
		if (object == null) {
			if (expectNonNull) {
				throw new RuntimeException("Error deserializing object to JSON: " + path + " is required but was undefined.");
			} else {
				return null;
			}
		}

		if (requiredType.isInstance(object)) {
			return requiredType.cast(object);
		}
		String expected = requiredType.getSimpleName();
		String actual = graphQLJsonSerializer.serialize(object);
		throw new RuntimeException("Error deserializing object to JSON: " + path + " has to be " + expected + " but found " + actual);
	}

	@SuppressWarnings("unchecked")
	private static <T> T uncheckedCast(Object object) {
		return (T) object;
	}
}
