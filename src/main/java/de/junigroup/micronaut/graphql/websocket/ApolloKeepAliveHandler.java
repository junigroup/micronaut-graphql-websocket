/*
 * Copyright 2019 original author
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.junigroup.micronaut.graphql.websocket;

import io.micronaut.websocket.WebSocketSession;

/**
 * The implementation of the keep-alive handler send the {@link ApolloOperationMessage.Type#GQL_KEEP_ALIVE} message at
 * periodic intervals.
 * 
 * @author Lars Niedziolka
 */
interface ApolloKeepAliveHandler {

	/**
	 * Adds the given session to the set of sessions that send out the keep alive events.
	 * <p>
	 * The first event will be send out directly in the call only to this session. The next events will be send out in a
	 * global periodic interval.
	 * 
	 * @param session the session that request the keep alive event.
	 */
	void register(WebSocketSession session);

	/**
	 * Removes the given session from the set of sessions that send out the keep alive events.
	 * 
	 * @param session the session that request the keep alive event.
	 */
	void deregister(WebSocketSession session);
}
