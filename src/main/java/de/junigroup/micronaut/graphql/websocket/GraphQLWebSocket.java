/*
 * Copyright 2019 original author
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.junigroup.micronaut.graphql.websocket;

import java.util.Optional;

import javax.inject.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpRequest;
import io.micronaut.websocket.CloseReason;
import io.micronaut.websocket.WebSocketSession;
import io.micronaut.websocket.annotation.OnClose;
import io.micronaut.websocket.annotation.OnError;
import io.micronaut.websocket.annotation.OnMessage;
import io.micronaut.websocket.annotation.OnOpen;
import io.micronaut.websocket.annotation.ServerWebSocket;

/**
 * Server to handle GraphQL subscriptions over WebSocket protocol.
 * <p>
 * The default entry point is {@code /subscriptions}.
 * 
 * @author Lars Niedziolka
 */
@ServerWebSocket("${" + GraphQLWebSocketConfiguration.PATH + ":" + GraphQLWebSocketConfiguration.DEFAULT_PATH + "}")
public class GraphQLWebSocket {
	private static final CloseReason NO_PROTOCOL_HANDLER_CLOSE_REASON = CloseReason.PROTOCOL_ERROR;
	private static final CloseReason ERROR_CLOSE_REASON = CloseReason.INTERNAL_ERROR;

	private static final String PROTOCOL_HANDLER_KEY = SubscriptionProtocolHandler.class.getName();

	private static final Logger logger = LoggerFactory.getLogger(GraphQLWebSocket.class);

	private final Provider<ApolloSubscriptionProtocolHandler> apolloSubscriptionProtocolHandlerProvider;

	public GraphQLWebSocket(Provider<ApolloSubscriptionProtocolHandler> apolloSubscriptionProtocolHandlerProvider) {
		this.apolloSubscriptionProtocolHandlerProvider = apolloSubscriptionProtocolHandlerProvider;
	}

	@OnOpen
	public void onOpen(WebSocketSession session, HttpRequest<Object> originalRequest) {
		// Currently the sub protocols are always initialized with null.
		// To work around this we fetch the sub protocol directly from the HTTP request headers.
		String subprotocol = originalRequest.getHeaders().get(HttpHeaders.SEC_WEBSOCKET_PROTOCOL, String.class, "");
		logger.debug("Session open: {}, {}", session.getId(), subprotocol);

		Optional<SubscriptionProtocolHandler> protocolHandler = createSubscriptionProtocolHandler(session, subprotocol);
		if (protocolHandler.isPresent()) {
			logger.debug("Subscription protocol handler: {}", protocolHandler.get().getClass().getName());
			session.put(PROTOCOL_HANDLER_KEY, protocolHandler.get());
		} else {
			logger.warn("Subscription protocol handler: subprotocol {} not supported", subprotocol);
			shutdownSession(session);
		}
	}

	/**
	 * Creates the {@link SubscriptionProtocolHandler}, to handle the {@link #onMessage(String, WebSocketSession) messages}.
	 * <p>
	 * Currently the only supported protocol handler is the {@link ApolloSubscriptionProtocolHandler}.
	 * 
	 * @param session the session to open
	 * @param subprotocol the {@link HttpHeaderNames#SEC_WEBSOCKET_PROTOCOL "sec-websocket-protocol"} header attribute
	 * @return {@link SubscriptionProtocolHandler} or {@link Optional#empty()}
	 */
	private Optional<SubscriptionProtocolHandler> createSubscriptionProtocolHandler(WebSocketSession session, String subprotocol) {
		switch (subprotocol) {
		case "graphql-ws":
			return Optional.of(apolloSubscriptionProtocolHandlerProvider.get());
		default:
			return Optional.empty();
		}
	}

	@OnClose
	public void onClose(WebSocketSession session, CloseReason closeReason) {
		logger.debug("Session closed: {}, {}", session.getId(), closeReason);
		Optional<SubscriptionProtocolHandler> protocolHandler = session.get(PROTOCOL_HANDLER_KEY, SubscriptionProtocolHandler.class);
		if (protocolHandler.isPresent()) {
			protocolHandler.get().shutdown(session);
		} else {
			logger.warn("Session {} without message handler", session.getId());
		}
	}

	@OnMessage
	public void onMessage(WebSocketSession session, String message) {
		Optional<SubscriptionProtocolHandler> protocolHandler = session.get(PROTOCOL_HANDLER_KEY, SubscriptionProtocolHandler.class);
		if (protocolHandler.isPresent()) {
			logger.trace("Handle session {} message: {}", session.getId(), message);
			try {
				protocolHandler.get().onMessage(session, message);
			} catch (RuntimeException e) {
				logger.error("Error executing websocket query for session: {}", session.getId(), e);
				closeUnexpectedly(session, e);
			}
		} else {
			logger.warn("Session {} without message handler", session.getId());
			shutdownSession(session);
		}
	}

	@OnError
	public void onError(WebSocketSession session, Throwable throwable) {
		logger.error("Error in websocket session: {}", session.getId(), throwable);
		closeUnexpectedly(session, throwable);
	}

	private void shutdownSession(WebSocketSession session) {
		logger.debug("Session shutdown: {}", NO_PROTOCOL_HANDLER_CLOSE_REASON);
		session.close(NO_PROTOCOL_HANDLER_CLOSE_REASON);
	}

	private void closeUnexpectedly(WebSocketSession session, Throwable e) {
		session.close(ERROR_CLOSE_REASON);
	}

}
