/*
 * Copyright 2019 original author
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.junigroup.micronaut.graphql.websocket;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Format of the messages to be received (client) and sent (server).
 * 
 * @author Lars Niedziolka
 * @see ApolloSubscriptionProtocolHandler for the apollo protocol
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApolloOperationMessage {
	/**
	 * The {@link ApolloOperationMessage#getType()} of the {@link ApolloOperationMessage}
	 * 
	 * @author Lars Niedziolka
	 */
	public enum Type {

		// Client Messages

		/** first contact => {@link #GQL_CONNECTION_ACK} + {@link #GQL_CONNECTION_KEEP_ALIVE} / {@link #GQL_CONNECTION_ERROR} */
		GQL_CONNECTION_INIT("connection_init"),

		/** bye bye => close session */
		GQL_CONNECTION_TERMINATE("connection_terminate"),

		/** query / subscription => {@link #GQL_DATA}* + {@link #GQL_COMPLETE} / {@link #GQL_ERROR} */
		GQL_START("start"),

		/** stop subscription => */
		GQL_STOP("stop"),

		// Server Messages

		/** {@link #GQL_START} => contact ok */
		GQL_CONNECTION_ACK("connection_ack"),

		/** {@link #GQL_START} => contact denied, close session */
		GQL_CONNECTION_ERROR("connection_error"), //

		/** {@link #GQL_START} => the next data */
		GQL_DATA("data"),

		/** {@link #GQL_START} => request not understood */
		GQL_ERROR("error"),

		/** {@link #GQL_START} => request completed, no more data available */
		GQL_COMPLETE("complete"),

		/** repeating message to keep the connection open */
		GQL_KEEP_ALIVE("ka");

		private final String type;

		Type(String type) {
			this.type = type;
		}

		@JsonValue
		public String getType() {
			return type;
		}
	}

	private Type type;
	private String id;
	private Object payload;

	/** Default constructor for JSON de-serialization. */
	public ApolloOperationMessage() {
	}

	/**
	 * Constructor to initialize operation message with arguments.
	 * 
	 * @param type the required type
	 * @param id the ID from
	 */
	public ApolloOperationMessage(Type type, String id, Object payload) {
		this.type = type;
		this.id = id;
		this.payload = payload;
	}

	public Type getType() {
		return type;
	}

	public String getId() {
		return id;
	}

	public Object getPayload() {
		return payload;
	}

	@Override
	public String toString() {
		return String.format("ApolloOperationMessage [type=%s, id=%s, payload=%s]", type, id, payload);
	}
}
