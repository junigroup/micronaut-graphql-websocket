/*
 * Copyright 2019 original author
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.junigroup.micronaut.graphql.websocket;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.qos.logback.classic.Level;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import io.leangen.graphql.GraphQLSchemaGenerator;
import io.micronaut.configuration.graphql.DefaultGraphQLExecutionInputCustomizer;
import io.micronaut.configuration.graphql.DefaultGraphQLExecutionResultHandler;
import io.micronaut.configuration.graphql.DefaultGraphQLInvocation;
import io.micronaut.configuration.graphql.GraphQLExecutionInputCustomizer;
import io.micronaut.configuration.graphql.GraphQLExecutionResultHandler;
import io.micronaut.configuration.graphql.GraphQLInvocation;
import io.micronaut.configuration.graphql.GraphQLJsonSerializer;
import io.micronaut.configuration.graphql.JacksonGraphQLJsonSerializer;

/**
 * Tests the {@link ApolloSubscriptionProtocolHandler} by directly calling.
 * <p>
 * WebSocket communication will be mocked.
 * 
 * @author Lars Niedziolka
 */
public class ApolloSubscriptionProtocolHandlerDirectTest {
	private static GraphQL graphQL;
	private ApolloKeepAliveStub keepAliveHandler;
	private ApolloSubscriptionProtocolHandler protocolHandler;

	@BeforeAll
	static void setupGraphQL() {
		((ch.qos.logback.classic.Logger) LoggerFactory.getLogger("graphql.execution")).setLevel(Level.ERROR);
		GraphQLSchema schema = new GraphQLSchemaGenerator().withOperationsFromSingleton(new GraphQLTestController()).generate();
		graphQL = new GraphQL.Builder(schema).build();
	}

	@BeforeEach
	void setup() {
		GraphQLExecutionInputCustomizer graphQLExecutionInputCustomizer = new DefaultGraphQLExecutionInputCustomizer();
		GraphQLInvocation graphQLInvocation = new DefaultGraphQLInvocation(graphQL, graphQLExecutionInputCustomizer);
		GraphQLExecutionResultHandler graphQLExecutionResultHandler = new DefaultGraphQLExecutionResultHandler();
		GraphQLJsonSerializer graphQLJsonSerializer = new JacksonGraphQLJsonSerializer(new ObjectMapper());
		keepAliveHandler = new ApolloKeepAliveStub();
		protocolHandler = new ApolloSubscriptionProtocolHandler(graphQLInvocation, graphQLExecutionResultHandler, graphQLJsonSerializer, keepAliveHandler);
	}

	@Test
	void testOnMessage_connectionInit_receiveConnectionAck() {
		WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

		protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));

		stub.recordTester.assertValues( //
				"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
				"\n[ID0000==] sendSync {'type':'ka'} application/json" //
		);
	}

	@Nested
	class SubscriptionOverWebsocket {

		@Test
		void testOnMessage_subscribeEmpty_receiveComplete() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'subscription{ serialEmpty{index} }'}}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json" //
			);
		}

		@Test
		void testOnMessage_subscribeSingle_receiveDataComplete() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'subscription{ serialSingle{index} }'}}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialSingle':{'index':1}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json" //
			);
		}

		@Test
		void testOnMessage_subscribeData5_receiveData5xComplete() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'subscription{ serialData5{index} }'}}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialData5':{'index':1}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialData5':{'index':2}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialData5':{'index':3}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialData5':{'index':4}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialData5':{'index':5}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json" //
			);
		}

		@Test
		void testOnMessage_subscribeError_receiveError() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'subscription{ serialError{index} }'}}"));

			String error = "{'message':'Error in serialError','locations':[]}";
			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'error','id':'1','payload':" + error + "} application/json" //
			);
		}

		@Test
		void testOnMessage_subscribeData5Error_receiveData5xError() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'subscription{ serialData5Error{index} }'}}"));

			String error = "{'message':'Error in serialData5Error','locations':[]}";
			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialData5Error':{'index':1}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialData5Error':{'index':2}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialData5Error':{'index':3}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialData5Error':{'index':4}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialData5Error':{'index':5}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'error','id':'1','payload':" + error + "} application/json" //
			);
		}

		@Test
		void testOnMessage_subscribeExecutionException_receiveDataWithErrorAndNullData() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'subscription{ serialExecutionException{index} }'}}"));

			String error = "{'message':'Exception while fetching data (/serialExecutionException) : Exception in serialExecutionException','locations':[{'line':1,'column':15}],'path':['serialExecutionException']}";
			stub.recordTester.assertValues(//
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'errors':[" + error + "],'data':null}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json" //
			);
		}

		@Test
		void testOnMessage_stopSerialSingleAfterComplete_receiveDataComplete() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'subscription{ serialSingle{index} }'}}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'stop'}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialSingle':{'index':1}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json" //
			);
		}

		@Test
		void testOnMessage_stopSerialDataDelayData_receiveFirstDataStop() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'subscription{ serialDataDelayData{index} }'}}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'stop'}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialDataDelayData':{'index':1}}}} application/json" //
			);
		}

		@Test
		void testOnMessage_connectionTerminateSerialSingleAfterComplete_receiveDataCompleteNormalClose() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'subscription{ serialSingle{index} }'}}"));
			protocolHandler.onMessage(stub.session, message("{'type':'connection_terminate'}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialSingle':{'index':1}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json", //
					"\n[ID0000==] close CloseReason{code=1000, reason='Normal Closure'}" //
			);
		}

		@Test
		void testOnMessage_connectionTerminateSerialDataDelayData_receiveFirstDataNormalClose() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'subscription{ serialDataDelayData{index} }'}}"));
			protocolHandler.onMessage(stub.session, message("{'type':'connection_terminate'}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialDataDelayData':{'index':1}}}} application/json", //
					"\n[ID0000==] close CloseReason{code=1000, reason='Normal Closure'}" //
			);
		}

		@Test
		void testShutdown_afterSerialSingleComplete_receiveDataCompleteClose() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'subscription{ serialSingle{index} }'}}"));
			protocolHandler.shutdown(stub.session);

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialSingle':{'index':1}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json", //
					"\n[ID0000==] close CloseReason{code=1001, reason='Going Away'}" //
			);
		}

		@Test
		void testShutdown_interruptSerialDataDelayData_receiveFirstDataCloseByServer() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'subscription{ serialDataDelayData{index} }'}}"));
			protocolHandler.shutdown(stub.session);

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialDataDelayData':{'index':1}}}} application/json", //
					"\n[ID0000==] close CloseReason{code=1001, reason='Going Away'}" //
			);
		}

		@Test
		void testShutdown_afterTerminate_closeOnce() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'subscription{ serialDataDelayData{index} }'}}"));
			protocolHandler.onMessage(stub.session, message("{'type':'connection_terminate'}"));
			protocolHandler.shutdown(stub.session);

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialDataDelayData':{'index':1}}}} application/json", //
					"\n[ID0000==] close CloseReason{code=1000, reason='Normal Closure'}" //
			);
		}
	}

	@Nested
	class QueryOverWebsocket {

		@Test
		void testOnMessage_queryEmpty_receiveEmptyArrayComplete() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'query{ serialEmpty{index} }'}}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialEmpty':[]}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json" //
			);
		}

		@Test
		void testOnMessage_synchronRequest_receiveSingleDataAsScalarComplete() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'query{ synchronRequest{index} }'}}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'synchronRequest':{'index':1}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json" //
			);
		}

		@Test
		void testOnMessage_asyncCompletableFuture_receiveSingleDataAsScalarComplete() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'query{ asyncCompletableFuture{index} }'}}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'asyncCompletableFuture':{'index':1}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json" //
			);
		}

		@Test
		void testOnMessage_querySingle_receiveArrayOfSingleDataComplete() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'query{ serialSingle{index} }'}}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialSingle':[{'index':1}]}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json" //
			);
		}

		@Test
		void testOnMessage_queryData5_receiveArrayOf5DataComplete() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'query{ serialData5{index} }'}}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialData5':[{'index':1},{'index':2},{'index':3},{'index':4},{'index':5}]}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json" //
			);
		}

		@Test
		void testOnMessage_queryError_receiveError() {
			SpqrVersion.assumeSupportsGraphQLVersion12();

			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'query{ serialError{index} }'}}"));

			String error = "{'message':'Error in serialError','locations':[]}";
			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'error','id':'1','payload':" + error + "} application/json" //
			);
		}

		@Test
		void testOnMessage_queryData5Error_receiveError() {
			SpqrVersion.assumeSupportsGraphQLVersion12();

			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'query{ serialData5Error{index} }'}}"));

			String error = "{'message':'Error in serialData5Error','locations':[]}";
			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'error','id':'1','payload':" + error + "} application/json" //
			);
		}

		@Test
		void testOnMessage_queryExecutionException_receiveDataWithErrorAndNullResultData() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'query{ serialExecutionException{index} }'}}"));

			String error = "{'message':'Exception while fetching data (/serialExecutionException) : Exception in serialExecutionException','locations':[{'line':1,'column':8}],'path':['serialExecutionException']}";
			stub.recordTester.assertValues(//
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'errors':[" + error
							+ "],'data':{'serialExecutionException':null}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json" //
			);
		}

		@Test
		void testOnMessage_stopSerialSingleAfterComplete_receiveArrayOfSingleDataComplete() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'query{ serialSingle{index} }'}}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'stop'}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialSingle':[{'index':1}]}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json" //
			);
		}

		@Test
		void testOnMessage_stopSerialDataDelayData_receiveNoData() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'query{ serialDataDelayData{index} }'}}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'stop'}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json" //
			);
		}

		@Test
		void testOnMessage_connectionTerminateSerialSingleAfterComplete_receiveArrayOfSingleDataCompleteNormalClose() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'query{ serialSingle{index} }'}}"));
			protocolHandler.onMessage(stub.session, message("{'type':'connection_terminate'}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialSingle':[{'index':1}]}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json", //
					"\n[ID0000==] close CloseReason{code=1000, reason='Normal Closure'}" //
			);
		}

		@Test
		void testOnMessage_connectionTerminateSerialDataBeforeComplete_receiveNoDataNormalClose() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'query{ serialDataDelayData{index} }'}}"));
			protocolHandler.onMessage(stub.session, message("{'type':'connection_terminate'}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] close CloseReason{code=1000, reason='Normal Closure'}" //
			);
		}

		@Test
		void testShutdown_afterSerialSingleComplete_receiveArrayOfSingleDataCompleteCloseByServer() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'query{ serialSingle{index} }'}}"));
			protocolHandler.shutdown(stub.session);

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialSingle':[{'index':1}]}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json", //
					"\n[ID0000==] close CloseReason{code=1001, reason='Going Away'}" //
			);
		}

		@Test
		void testShutdown_interruptSerialDataBeforeComplete_receiveNoDataCloseByServer() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'query{ serialDataDelayData{index} }'}}"));
			protocolHandler.shutdown(stub.session);

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] close CloseReason{code=1001, reason='Going Away'}" //
			);
		}

		@Test
		void testShutdown_afterTerminate_closeOnce() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'query{ serialDataDelayData{index} }'}}"));
			protocolHandler.onMessage(stub.session, message("{'type':'connection_terminate'}"));
			protocolHandler.shutdown(stub.session);

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] close CloseReason{code=1000, reason='Normal Closure'}" //
			);
		}
	}

	@Nested
	class MutationOverWebsocket {

		@Test
		void testOnMessage_mutateWithEmptyResult_receiveEmptyArrayComplete() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'mutation{ serialEmpty{index} }'}}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialEmpty':[]}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json" //
			);
		}

		@Test
		void testOnMessage_synchronMutation_receiveSingleDataAsScalarComplete() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'mutation{ synchronRequest{index} }'}}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'synchronRequest':{'index':1}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json" //
			);
		}

		@Test
		void testOnMessage_mutationWithAsyncCompletableFuture_receiveSingleDataAsScalarComplete() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'mutation{ asyncCompletableFuture{index} }'}}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'asyncCompletableFuture':{'index':1}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json" //
			);
		}

		@Test
		void testOnMessage_mutationWithSingleResult_receiveArrayOfSingleDataComplete() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'mutation{ serialSingle{index} }'}}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialSingle':[{'index':1}]}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json" //
			);
		}

		@Test
		void testOnMessage_mutationWith5Results_receiveArrayOf5DataComplete() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'mutation{ serialData5{index} }'}}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialData5':[{'index':1},{'index':2},{'index':3},{'index':4},{'index':5}]}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json" //
			);
		}

		@Test
		void testOnMessage_mutationWithPublisherError_receiveError() {
			SpqrVersion.assumeSupportsGraphQLVersion12();

			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'mutation{ serialError{index} }'}}"));

			String error = "{'message':'Error in serialError','locations':[]}";
			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'error','id':'1','payload':" + error + "} application/json" //
			);
		}

		@Test
		void testOnMessage_mutationWithPublisher5ResultsAndError_receiveError() {
			SpqrVersion.assumeSupportsGraphQLVersion12();

			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'mutation{ serialData5Error{index} }'}}"));

			String error = "{'message':'Error in serialData5Error','locations':[]}";
			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'error','id':'1','payload':" + error + "} application/json" //
			);
		}

		@Test
		void testOnMessage_mutationExecutionException_receiveDataWithErrorAndNullResultData() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'mutation{ serialExecutionException{index} }'}}"));

			String error = "{'message':'Exception while fetching data (/serialExecutionException) : Exception in serialExecutionException','locations':[{'line':1,'column':11}],'path':['serialExecutionException']}";
			stub.recordTester.assertValues(//
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'errors':[" + error
							+ "],'data':{'serialExecutionException':null}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json" //
			);
		}

		@Test
		void testOnMessage_stopSerialSingleAfterComplete_receiveArrayOfSingleDataComplete() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'mutation{ serialSingle{index} }'}}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'stop'}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialSingle':[{'index':1}]}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json" //
			);
		}

		@Test
		void testOnMessage_stopSerialDataDelayData_receiveNoData() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'mutation{ serialDataDelayData{index} }'}}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'stop'}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json" //
			);
		}

		@Test
		void testOnMessage_connectionTerminateSerialSingleAfterComplete_receiveArrayOfSingleDataCompleteNormalClose() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'mutation{ serialSingle{index} }'}}"));
			protocolHandler.onMessage(stub.session, message("{'type':'connection_terminate'}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialSingle':[{'index':1}]}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json", //
					"\n[ID0000==] close CloseReason{code=1000, reason='Normal Closure'}" //
			);
		}

		@Test
		void testOnMessage_connectionTerminateSerialDataBeforeComplete_receiveNoDataNormalClose() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'mutation{ serialDataDelayData{index} }'}}"));
			protocolHandler.onMessage(stub.session, message("{'type':'connection_terminate'}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] close CloseReason{code=1000, reason='Normal Closure'}" //
			);
		}

		@Test
		void testShutdown_afterSerialSingleComplete_receiveArrayOfSingleDataCompleteCloseByServer() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'mutation{ serialSingle{index} }'}}"));
			protocolHandler.shutdown(stub.session);

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialSingle':[{'index':1}]}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json", //
					"\n[ID0000==] close CloseReason{code=1001, reason='Going Away'}" //
			);
		}

		@Test
		void testShutdown_interruptSerialDataBeforeComplete_receiveNoDataCloseByServer() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'mutation{ serialDataDelayData{index} }'}}"));
			protocolHandler.shutdown(stub.session);

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] close CloseReason{code=1001, reason='Going Away'}" //
			);
		}

		@Test
		void testShutdown_afterTerminate_closeOnce() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'mutation{ serialDataDelayData{index} }'}}"));
			protocolHandler.onMessage(stub.session, message("{'type':'connection_terminate'}"));
			protocolHandler.shutdown(stub.session);

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] close CloseReason{code=1000, reason='Normal Closure'}" //
			);
		}
	}

	@Nested
	class KeepAlive {
		@Test
		void testKeepAlive_afterConnectionInit_firstKeepAlive() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json" //
			);
		}

		@Test
		void testKeepAlive_keepAliveHandlerTriggersTimer_nextKeepAlive() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			keepAliveHandler.triggerTimer();

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json" //
			);
		}

		@Test
		void testKeepAlive_keepAliveHandlerBetweenRequests_nextKeepAliveBetweenResponse() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'subscription{ serialSingle{index} }'}}"));
			keepAliveHandler.triggerTimer();
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'subscription{ serialSingle{index} }'}}"));

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialSingle':{'index':1}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialSingle':{'index':1}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json" //
			);
		}

		@Test
		void testKeepAlive_keepAliveHandlerBetweenLongRunningRequests_nextKeepAliveBetweenResponse() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'id':'1','type':'start','payload':{'query':'subscription{ serialDataDelayData{index} }'}}"));
			keepAliveHandler.triggerTimer();

			stub.recordTester.awaitCount(6);
			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialDataDelayData':{'index':1}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] sendSync {'type':'data','id':'1','payload':{'data':{'serialDataDelayData':{'index':2}}}} application/json", //
					"\n[ID0000==] sendSync {'type':'complete','id':'1'} application/json" //
			);
		}

		@Test
		void testKeepAlive_afterConnectionTerminate_noMoreKeepAlive() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.onMessage(stub.session, message("{'type':'connection_terminate'}"));
			keepAliveHandler.triggerTimer();

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] close CloseReason{code=1000, reason='Normal Closure'}" //
			);
		}

		@Test
		void testKeepAlive_afterShutdown_noMoreKeepAlive() {
			WebSocketSessionStub stub = new WebSocketSessionStub("ID0000==");

			protocolHandler.onMessage(stub.session, message("{'type':'connection_init'}"));
			protocolHandler.shutdown(stub.session);
			keepAliveHandler.triggerTimer();

			stub.recordTester.assertValues( //
					"\n[ID0000==] sendSync {'type':'connection_ack'} application/json", //
					"\n[ID0000==] sendSync {'type':'ka'} application/json", //
					"\n[ID0000==] close CloseReason{code=1001, reason='Going Away'}" //
			);
		}

	}

	private static String message(String string) {
		return string.replace('\'', '"');
	}

}
