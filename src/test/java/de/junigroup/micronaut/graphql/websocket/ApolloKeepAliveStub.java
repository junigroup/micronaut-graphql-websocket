/*
 * Copyright 2019 original author
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.junigroup.micronaut.graphql.websocket;

import java.util.concurrent.ConcurrentLinkedQueue;

import io.micronaut.http.MediaType;
import io.micronaut.websocket.WebSocketSession;

/**
 * Test implementation of {@link ApolloKeepAliveHandler} that triggers the keep alive events on call of
 * {@link #triggerTimer()}.
 * 
 * @author Lars Niedziolka
 */
public class ApolloKeepAliveStub implements ApolloKeepAliveHandler {
	private static final String KEEP_ALIVE_MESSAGE = "{\"type\":\"ka\"}";
	private final ConcurrentLinkedQueue<WebSocketSession> sessions = new ConcurrentLinkedQueue<>();

	@Override
	public void register(WebSocketSession session) {
		sendKeepAlive(session);
		sessions.add(session);
	}

	@Override
	public void deregister(WebSocketSession session) {
		sessions.remove(session);
	}

	/** Trigger the timer programmatically */
	public void triggerTimer() {
		sessions.forEach(this::sendKeepAlive);
	}

	private void sendKeepAlive(WebSocketSession session) {
		session.sendSync(KEEP_ALIVE_MESSAGE, MediaType.APPLICATION_JSON_TYPE);
	}

}
