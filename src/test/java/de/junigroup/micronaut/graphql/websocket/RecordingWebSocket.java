package de.junigroup.micronaut.graphql.websocket;

import io.micronaut.websocket.CloseReason;
import io.micronaut.websocket.WebSocketSession;
import io.micronaut.websocket.annotation.ClientWebSocket;
import io.micronaut.websocket.annotation.OnClose;
import io.micronaut.websocket.annotation.OnError;
import io.micronaut.websocket.annotation.OnMessage;
import io.micronaut.websocket.annotation.OnOpen;
import io.micronaut.websocket.interceptor.WebSocketSessionAware;
import io.reactivex.BackpressureStrategy;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subscribers.TestSubscriber;

/**
 * Implements a {@link ClientWebSocket} that is recording open and close actions and sent and received messages.
 * 
 * @author Lars Niedziolka
 */
@ClientWebSocket
public class RecordingWebSocket implements AutoCloseable, WebSocketSessionAware {
	final PublishSubject<String> recorder;
	final TestSubscriber<String> tester;
	WebSocketSession session;

	public RecordingWebSocket() {
		this.recorder = PublishSubject.create();
		this.tester = new TestSubscriber<String>();
		this.recorder.toFlowable(BackpressureStrategy.BUFFER).subscribe(this.tester);
	}

	/* Eclipse compilations requires (and) calls this method */
	@Override
	public void setWebSocketSession(WebSocketSession session) {
	}

	@OnOpen
	public void onOpen(WebSocketSession session) {
		this.session = session;
		record("open");
	}

	@OnClose
	public void onClose(CloseReason reason) {
		record("close: ", reason);
		terminated();
	}

	public void sendSync(String message) {
		record("message > ", message.replace('"', '\''));
		session.sendSync(message);
	}

	@OnMessage
	public void onMessage(String message) {
		record("message < ", message.replace('"', '\''));
	}

	@OnError
	public void onError(Throwable throwable) {
		record("error: ", throwable);
		terminated();
	}

	private void record(String signal) {
		recorder.onNext(signal);
	}

	private void record(String signal, Object payload) {
		recorder.onNext(signal + payload);
	}

	@Override
	public void close() {
		if (session != null) {
			session.close();
		}
	}

	private void terminated() {
		this.session = null;
		this.recorder.onComplete();
	}
}