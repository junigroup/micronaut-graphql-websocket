/*
 * Copyright 2019 original author
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.junigroup.micronaut.graphql.websocket;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.clearInvocations;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.time.Duration;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.stubbing.Answer;
import org.reactivestreams.Publisher;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.junigroup.micronaut.graphql.websocket.TaskSchedulerMock.ScheduledTask;
import io.micronaut.configuration.graphql.GraphQLJsonSerializer;
import io.micronaut.configuration.graphql.JacksonGraphQLJsonSerializer;
import io.micronaut.core.convert.value.MutableConvertibleValuesMap;
import io.micronaut.http.MediaType;
import io.micronaut.websocket.WebSocketBroadcaster;
import io.micronaut.websocket.WebSocketSession;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;

/**
 * Test the {@link ApolloKeepAliveTicker} directly with mocked {@link WebSocketSession}s.
 * 
 * @author Lars Niedziolka
 */
public class ApolloKeepAliveTickerDirectlyTest {
	static final String KEEP_ALIVE_MESSAGE = "{\"type\":\"ka\"}";

	GraphQLJsonSerializer graphQLJsonSerializer = new JacksonGraphQLJsonSerializer(new ObjectMapper());

	WebSocketBroadcaster broadcaster = broatcasterTo();

	TaskSchedulerMock scheduler = new TaskSchedulerMock();

	Duration period = Duration.ofSeconds(15);

	@Test
	void testApolloKeepAliveTicker_noRegistration_noSchedulerInteraction() {
		TaskSchedulerMock schedulerSpy = spy(scheduler);

		new ApolloKeepAliveTicker(graphQLJsonSerializer, broadcaster, schedulerSpy, period);

		verifyZeroInteractions(schedulerSpy);
	}

	@Test
	void testApolloKeepAliveTicker_noRegistration_noBroadcasterInteraction() {
		WebSocketBroadcaster broadcasterSpy = spy(broadcaster);

		new ApolloKeepAliveTicker(graphQLJsonSerializer, broadcasterSpy, scheduler, period);

		verifyZeroInteractions(broadcasterSpy);
	}

	@Test
	void testRegister_keepAliveMessage_directlyOnRegistration() {
		WebSocketSession session = mockSession("xxxxIDxxxx-001");
		ApolloKeepAliveTicker keepAliveTicker = new ApolloKeepAliveTicker(graphQLJsonSerializer, broadcaster, scheduler, period);

		keepAliveTicker.register(session);

		List<String> messages = extractSentJsonMessages(session);
		assertEquals(asList(KEEP_ALIVE_MESSAGE), messages);
	}

	@Test
	void testRegister_firstRegistration_startSchedulerAtFixedRate() {
		Duration period = Duration.ofSeconds(5);
		ApolloKeepAliveTicker keepAliveTicker = new ApolloKeepAliveTicker(graphQLJsonSerializer, broadcaster, scheduler, period);
		WebSocketSession session = mockSession("xxxxIDxxxx-001");

		keepAliveTicker.register(session);

		assertEquals(scheduler.scheduledTasks.size(), 1, "number of scheduled tasks");
		assertEquals("delay:PT5S,fixed rate:PT5S", scheduler.scheduledTasks.get(0).timing, "#0 timing");
	}

	@Test
	void testRegister_secondRegistration_remainsOnlyOneScheduledTask() {
		WebSocketSession session1 = mockSession("xxxxIDxxxx-001");
		WebSocketSession session2 = mockSession("xxxxIDxxxx-002");
		ApolloKeepAliveTicker keepAliveTicker = new ApolloKeepAliveTicker(graphQLJsonSerializer, broadcaster, scheduler, period);
		keepAliveTicker.register(session1);

		keepAliveTicker.register(session2);

		assertEquals(scheduler.scheduledTasks.size(), 1, "number of scheduled tasks");
	}

	@Test
	void testTriggerAtFixedRateCommand_oneSession_broadcastKeepAliveToSession() {
		WebSocketSession session = mockSession("xxxxIDxxxx-001");
		broadcaster = broatcasterTo(session);
		ApolloKeepAliveTicker keepAliveTicker = new ApolloKeepAliveTicker(graphQLJsonSerializer, broadcaster, scheduler, period);
		keepAliveTicker.register(session);
		clearInvocations(session);

		scheduler.triggerAll();

		List<String> messages = extractSentJsonMessages(session);
		assertEquals(asList(KEEP_ALIVE_MESSAGE), messages);
	}

	@Test
	void testTriggerAtFixedRateCommand_twoSessions_broadcastKeepAliveToBootSession() {
		WebSocketSession session1 = mockSession("xxxxIDxxxx-001");
		WebSocketSession session2 = mockSession("xxxxIDxxxx-002");
		broadcaster = broatcasterTo(session1, session2);
		ApolloKeepAliveTicker keepAliveTicker = new ApolloKeepAliveTicker(graphQLJsonSerializer, broadcaster, scheduler, period);
		keepAliveTicker.register(session1);
		keepAliveTicker.register(session2);
		clearInvocations(session1, session2);

		scheduler.triggerAll();

		List<String> messages1 = extractSentJsonMessages(session1);
		List<String> messages2 = extractSentJsonMessages(session2);
		assertEquals(asList(KEEP_ALIVE_MESSAGE), messages1);
		assertEquals(asList(KEEP_ALIVE_MESSAGE), messages2);
	}

	@Test
	void testDeregister_nextToLastSession_scheduledFutureNeverCanceled() {
		WebSocketSession session1 = mockSession("xxxxIDxxxx-001");
		WebSocketSession session2 = mockSession("xxxxIDxxxx-002");
		ApolloKeepAliveTicker keepAliveTicker = new ApolloKeepAliveTicker(graphQLJsonSerializer, broadcaster, scheduler, period);
		keepAliveTicker.register(session1);
		keepAliveTicker.register(session2);
		ScheduledTask scheduledTask = scheduler.scheduledTasks.get(0);

		keepAliveTicker.deregister(session1);

		verify(scheduledTask.future, never()).cancel(anyBoolean());
	}

	@Test
	void testDeregister_lastSession_scheduledTaskCanceled() {
		WebSocketSession session1 = mockSession("xxxxIDxxxx-001");
		WebSocketSession session2 = mockSession("xxxxIDxxxx-002");
		ApolloKeepAliveTicker keepAliveTicker = new ApolloKeepAliveTicker(graphQLJsonSerializer, broadcaster, scheduler, period);
		keepAliveTicker.register(session1);
		keepAliveTicker.register(session2);
		keepAliveTicker.deregister(session1);
		ScheduledTask scheduledTask = scheduler.scheduledTasks.get(0);

		keepAliveTicker.deregister(session2);

		verify(scheduledTask.future, times(1)).cancel(false);
	}

	private static WebSocketSession mockSession(String id) {
		MutableConvertibleValuesMap<Object> valuesMap = new MutableConvertibleValuesMap<Object>();
		Answer<?> delegateToValuesMap = invocation -> invocation.getMethod().invoke(valuesMap, invocation.getArguments());

		WebSocketSession session = mock(WebSocketSession.class);
		when(session.getId()).thenReturn(id);
		when(session.get(any(), any(), any())).then(delegateToValuesMap);
		when(session.put(any(), any())).then(delegateToValuesMap);
		return session;
	}

	private static WebSocketBroadcaster broatcasterTo(WebSocketSession... sessions) {
		class WebSocketBroadcasterImplementation implements WebSocketBroadcaster {
			@Override
			public <T> Publisher<T> broadcast(T message, MediaType mediaType, Predicate<WebSocketSession> filter) {
				return Flowable.create(ermitter -> {
					Stream.of(sessions).filter(filter).forEach(session -> session.sendSync(message, mediaType));
					ermitter.onComplete();
				}, BackpressureStrategy.BUFFER);
			}
		}
		return new WebSocketBroadcasterImplementation();
	}

	private static List<String> extractSentJsonMessages(WebSocketSession session) {
		ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);
		verify(session, times(1)).sendSync(messageCaptor.capture(), eq(MediaType.APPLICATION_JSON_TYPE));
		return messageCaptor.getAllValues();
	}

}
