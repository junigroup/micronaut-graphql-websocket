package de.junigroup.micronaut.graphql.websocket;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Duration;

import javax.inject.Inject;

import org.junit.jupiter.api.Test;

import io.micronaut.context.annotation.Property;
import io.micronaut.test.annotation.MicronautTest;

public class ApolloKeepAliveHandlerConfigurationTest {

	@MicronautTest
	static class DefaultConfiguration {
		@Inject
		ApolloKeepAliveHandler apolloKeepAliveHandler;

		@Test
		void testApolloKeepAliveHandler_defaultConfiguration_isApolloKeepAliveTicker() {
			assertNotNull(apolloKeepAliveHandler);
			assertEquals(ApolloKeepAliveTicker.class, apolloKeepAliveHandler.getClass());
		}

		@Test
		void testApolloKeepAliveHandler_defaultConfiguration_periodIsDefault() {
			Duration defaultKeepAlive = Duration.parse(GraphQLWebSocketConfiguration.DEFAULT_KEEP_ALIVE);
			ApolloKeepAliveTicker apolloKeepAliveTicker = (ApolloKeepAliveTicker) apolloKeepAliveHandler;
			assertEquals(defaultKeepAlive, apolloKeepAliveTicker.getPeriod());
		}

		@Test
		void testApolloKeepAliveHandler_defaultConfiguration_isEnabled() {
			ApolloKeepAliveTicker apolloKeepAliveTicker = (ApolloKeepAliveTicker) apolloKeepAliveHandler;
			assertTrue(apolloKeepAliveTicker.isEnabled());
		}
	}

	@MicronautTest
	@Property(name = GraphQLWebSocketConfiguration.KEEP_ALIVE, value = "1h")
	static class SetByConfiguration {
		@Inject
		ApolloKeepAliveHandler apolloKeepAliveHandler;

		@Test
		void testApolloKeepAliveHandler_setByConfiguration_isApolloKeepAliveTicker() {
			assertNotNull(apolloKeepAliveHandler);
			assertEquals(ApolloKeepAliveTicker.class, apolloKeepAliveHandler.getClass());
		}

		@Test
		void testApolloKeepAliveHandler_setByConfiguration_periodIsZero() {
			ApolloKeepAliveTicker apolloKeepAliveTicker = (ApolloKeepAliveTicker) apolloKeepAliveHandler;
			assertEquals(Duration.ofHours(1), apolloKeepAliveTicker.getPeriod());
		}

		@Test
		void testApolloKeepAliveHandler_setByConfiguration_isEnabled() {
			ApolloKeepAliveTicker apolloKeepAliveTicker = (ApolloKeepAliveTicker) apolloKeepAliveHandler;
			assertTrue(apolloKeepAliveTicker.isEnabled());
		}
	}

	@MicronautTest
	@Property(name = GraphQLWebSocketConfiguration.KEEP_ALIVE, value = "0ms")
	static class DisabledConfiguration {
		@Inject
		ApolloKeepAliveHandler apolloKeepAliveHandler;

		@Test
		void testApolloKeepAliveHandler_disabledByConfiguration_isApolloKeepAliveTicker() {
			assertNotNull(apolloKeepAliveHandler);
			assertEquals(ApolloKeepAliveTicker.class, apolloKeepAliveHandler.getClass());
		}

		@Test
		void testApolloKeepAliveHandler_disabledByConfiguration_periodIsZero() {
			ApolloKeepAliveTicker apolloKeepAliveTicker = (ApolloKeepAliveTicker) apolloKeepAliveHandler;
			assertEquals(Duration.ZERO, apolloKeepAliveTicker.getPeriod());
		}

		@Test
		void testApolloKeepAliveHandler_disabledByConfiguration_isNotEnabled() {
			ApolloKeepAliveTicker apolloKeepAliveTicker = (ApolloKeepAliveTicker) apolloKeepAliveHandler;
			assertFalse(apolloKeepAliveTicker.isEnabled());
		}
	}
}
