/*
 * Copyright 2019 original author
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.junigroup.micronaut.graphql.websocket;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.mock;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledFuture;

import org.mockito.Mockito;

import io.micronaut.scheduling.TaskScheduler;

/**
 * A mock of {@link TaskScheduler}, which allows to {@link #triggerAll() trigger} the {@link ScheduledTask
 * scheduledFuture}s programmatically.
 * 
 * @author Lars Niedziolka
 */
class TaskSchedulerMock implements TaskScheduler {
	final List<ScheduledTask> scheduledTasks = new ArrayList<>();

	class ScheduledTask {
		final String timing;
		final Callable<?> command;
		final ScheduledFuture<?> future;

		public ScheduledTask(String timing, Callable<?> command) {
			this.timing = timing;
			this.command = command;
			this.future = mock(ScheduledFuture.class);

			scheduledTasks.add(this);
			Mockito.doAnswer(invocation -> scheduledTasks.remove(this)).when(this.future).cancel(anyBoolean());
		}

		void trigger() {
			assertDoesNotThrow(() -> command.call());
		}
	}

	@Override
	public ScheduledFuture<?> schedule(String cron, Runnable command) {
		return addSchedulerFuture("cron:" + cron, command);
	}

	@Override
	public <V> ScheduledFuture<V> schedule(String cron, Callable<V> command) {
		return addSchedulerFuture("cron:" + cron, command);
	}

	@Override
	public ScheduledFuture<?> schedule(Duration delay, Runnable command) {
		return addSchedulerFuture("delay:" + delay, command);
	}

	@Override
	public <V> ScheduledFuture<V> schedule(Duration delay, Callable<V> callable) {
		return addSchedulerFuture("delay:" + delay, callable);
	}

	@Override
	public ScheduledFuture<?> scheduleAtFixedRate(Duration initialDelay, Duration period, Runnable command) {
		return addSchedulerFuture("delay:" + initialDelay + ",fixed rate:" + period, command);
	}

	@Override
	public ScheduledFuture<?> scheduleWithFixedDelay(Duration initialDelay, Duration delay, Runnable command) {
		return addSchedulerFuture("delay:" + initialDelay + ",fixed delay:" + delay, command);
	}

	private ScheduledFuture<?> addSchedulerFuture(String timing, Runnable command) {
		return addSchedulerFuture(timing, () -> {
			command.run();
			return null;
		});
	}

	@SuppressWarnings("unchecked")
	private <V> ScheduledFuture<V> addSchedulerFuture(String timing, Callable<V> command) {
		ScheduledTask scheduledTask = new ScheduledTask(timing, command);
		return (ScheduledFuture<V>) scheduledTask.future;
	}

	void triggerAll() {
		for (ScheduledTask scheduledFutureMock : scheduledTasks) {
			scheduledFutureMock.trigger();
		}
	}

}