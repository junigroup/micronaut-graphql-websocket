/*
 * Copyright 2019 original author
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.junigroup.micronaut.graphql.websocket;

import static org.mockito.Mockito.mock;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.mockito.Answers;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import io.micronaut.websocket.WebSocketSession;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.subscribers.TestSubscriber;

/**
 * Simple stubbing {@link WebSocketSession} with recording over {@link Flowable} in {@link #recordBuffer}.
 * <p>
 * The recording is replacing double quotes with single quotes to simplify the compare in java.
 * <p>
 * Records:
 * <ul>
 * <li>{@link WebSocketSession#isOpen()}</li>
 * <li>{@link WebSocketSession#close(io.micronaut.websocket.CloseReason)}</li>
 * <li>{@link WebSocketSession#sendSync(Object, io.micronaut.http.MediaType)}</li>
 * </ul>
 * 
 * @author Lars Niedziolka
 */
class WebSocketSessionStub {
	final String sessionId;
	final AtomicBoolean sessionIsOpen;
	final WebSocketSession session;
	final Flowable<String> recordBuffer;
	final TestSubscriber<String> recordTester;

	WebSocketSessionStub(String sessionId) {
		this.sessionId = sessionId;
		this.sessionIsOpen = new AtomicBoolean(true);
		this.session = stubSession(sessionId, sessionIsOpen);
		this.recordBuffer = stubSessionRecording(session, sessionId, sessionIsOpen);
		this.recordTester = subscribeTester(recordBuffer);
	}

	private static WebSocketSession stubSession(String sessionId, AtomicBoolean sessionIsOpen) {
		WebSocketSession webSocketSession = mock(WebSocketSession.class, Answers.RETURNS_DEEP_STUBS);
		Mockito.when(webSocketSession.getId()).thenReturn(sessionId);
		Mockito.when(webSocketSession.isOpen()).thenAnswer(call -> sessionIsOpen.get());
		return webSocketSession;
	}

	private static Flowable<String> stubSessionRecording(WebSocketSession session, String sessionId, AtomicBoolean sessionIsOpen) {
		return Flowable.<String>create(emitter -> {
			Answer<Void> doRecord = call -> recordSessionCall(sessionId, emitter, call);
			Consumer<InvocationOnMock> setClosed = call -> sessionIsOpen.set(false);

			Mockito.doAnswer(doAndAnswer(setClosed, doRecord)).when(session).close(ArgumentMatchers.any());
			Mockito.doAnswer(doRecord).when(session).sendSync(ArgumentMatchers.any(), ArgumentMatchers.any());
		}, BackpressureStrategy.BUFFER);
	}

	private static <T> Answer<T> doAndAnswer(Consumer<InvocationOnMock> task, Answer<T> answer) {
		return call -> {
			task.accept(call);
			return answer.answer(call);
		};
	}

	private static Void recordSessionCall(String sessionId, FlowableEmitter<String> emitter, InvocationOnMock call) {
		String arguments = Stream.of(call.getArguments()).map(it -> it.toString().replace('"', '\'')).collect(Collectors.joining(" "));
		String record = "\n[" + sessionId + "] " + call.getMethod().getName() + " " + arguments;
		emitter.onNext(record);
		return null;
	}

	private static TestSubscriber<String> subscribeTester(Flowable<String> recordBuffer) {
		TestSubscriber<String> recordSubscriber = new TestSubscriber<>();
		recordBuffer.subscribe(recordSubscriber);
		return recordSubscriber;
	}
}
