/*
 * Copyright 2019 original author
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.junigroup.micronaut.graphql.websocket;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;

import java.time.Duration;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.micronaut.configuration.graphql.GraphQLJsonSerializer;
import io.micronaut.configuration.graphql.JacksonGraphQLJsonSerializer;
import io.micronaut.scheduling.TaskScheduler;
import io.micronaut.websocket.WebSocketBroadcaster;
import io.micronaut.websocket.WebSocketSession;

/**
 * Test the {@link ApolloKeepAliveTicker} with <code>0s</code> period to disable the ticker.
 * 
 * @author Lars Niedziolka
 */
public class ApolloKeepAliveTickerDisabledTest {
	GraphQLJsonSerializer graphQLJsonSerializer = new JacksonGraphQLJsonSerializer(new ObjectMapper());

	WebSocketBroadcaster broadcaster = mock(WebSocketBroadcaster.class);

	TaskScheduler scheduler = mock(TaskScheduler.class);

	Duration zeroPeriod = Duration.ZERO;

	@Test
	void testApolloKeepAliveTicker_zeroPeriod_isNotEnabled() {
		ApolloKeepAliveTicker keepAliveTicker = new ApolloKeepAliveTicker(graphQLJsonSerializer, broadcaster, scheduler, zeroPeriod);

		assertFalse(keepAliveTicker.isEnabled(), "keep alive ticker is enabled");
	}

	@Test
	void testApolloKeepAliveTicker_noRegistration_noSchedulerInteraction() {
		new ApolloKeepAliveTicker(graphQLJsonSerializer, broadcaster, scheduler, zeroPeriod);

		verifyZeroInteractions(scheduler);
	}

	@Test
	void testApolloKeepAliveTicker_noRegistration_noBroadcasterInteraction() {
		new ApolloKeepAliveTicker(graphQLJsonSerializer, broadcaster, scheduler, zeroPeriod);

		verifyZeroInteractions(broadcaster);
	}

	@Test
	void testRegister_onRegistration_nothingSendOverSocket() {
		WebSocketSession session = mock(WebSocketSession.class, "xxxxIDxxxx-001");
		ApolloKeepAliveTicker keepAliveTicker = new ApolloKeepAliveTicker(graphQLJsonSerializer, broadcaster, scheduler, zeroPeriod);

		keepAliveTicker.register(session);

		verifyZeroInteractions(session);
	}

	@Test
	void testRegister_onRegistration_nothingSendOverBroadcaster() {
		WebSocketSession session = mock(WebSocketSession.class, "xxxxIDxxxx-001");
		ApolloKeepAliveTicker keepAliveTicker = new ApolloKeepAliveTicker(graphQLJsonSerializer, broadcaster, scheduler, zeroPeriod);

		keepAliveTicker.register(session);

		verifyZeroInteractions(broadcaster);
	}

	@Test
	void testRegister_onRegistration_schedulerNotStarted() {
		ApolloKeepAliveTicker keepAliveTicker = new ApolloKeepAliveTicker(graphQLJsonSerializer, broadcaster, scheduler, zeroPeriod);
		WebSocketSession session = mock(WebSocketSession.class, "xxxxIDxxxx-001");

		keepAliveTicker.register(session);

		verifyZeroInteractions(scheduler);
	}

	@Test
	void testDeregister_registeredSession_noInteractionWithSession() {
		WebSocketSession session = mock(WebSocketSession.class, "xxxxIDxxxx-001");
		ApolloKeepAliveTicker keepAliveTicker = new ApolloKeepAliveTicker(graphQLJsonSerializer, broadcaster, scheduler, zeroPeriod);
		keepAliveTicker.register(session);

		keepAliveTicker.deregister(session);

		verifyZeroInteractions(session);
	}

	@Test
	void testDeregister_registeredSession_noInteractionWithScheduler() {
		WebSocketSession session = mock(WebSocketSession.class, "xxxxIDxxxx-001");
		ApolloKeepAliveTicker keepAliveTicker = new ApolloKeepAliveTicker(graphQLJsonSerializer, broadcaster, scheduler, zeroPeriod);
		keepAliveTicker.register(session);

		keepAliveTicker.deregister(session);

		verifyZeroInteractions(scheduler);
	}

	@Test
	void testDeregister_registeredSession_noInteractionWithBroadcaster() {
		WebSocketSession session = mock(WebSocketSession.class, "xxxxIDxxxx-001");
		ApolloKeepAliveTicker keepAliveTicker = new ApolloKeepAliveTicker(graphQLJsonSerializer, broadcaster, scheduler, zeroPeriod);
		keepAliveTicker.register(session);

		keepAliveTicker.deregister(session);

		verifyZeroInteractions(broadcaster);
	}
}
