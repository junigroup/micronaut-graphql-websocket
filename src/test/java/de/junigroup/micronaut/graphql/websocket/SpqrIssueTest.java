package de.junigroup.micronaut.graphql.websocket;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.GraphQLError;
import graphql.schema.GraphQLSchema;
import io.leangen.graphql.GraphQLSchemaGenerator;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.annotations.GraphQLSubscription;
import io.micronaut.core.async.publisher.Publishers;
import io.reactivex.Flowable;

/**
 * {@link GraphQLQuery} query to method returning a publisher. </br>
 * {@link GraphQLMutation} mutation to method returning a publisher. </br>
 * {@link GraphQLSubscription} subscription to method returning a publisher. </br>
 * 
 * @see <a href="https://github.com/leangen/graphql-spqr/issues/264">graphql-spqr issue 264</a>
 * @see ApolloSubscriptionProtocolHandlerDirectTest#MAYBE_ERROR_IN_SPQR
 */
public class SpqrIssueTest {
	private GraphQL graphQL;

	public static class QueryService {
		/** query should return value */
		@GraphQLQuery
		@GraphQLMutation
		public Publisher<String> valuePublisher() {
			return Publishers.just("value over publisher");
		}

		/** query should fail with {@link GraphQLError} */
		@GraphQLQuery
		@GraphQLMutation
		@GraphQLSubscription
		public Publisher<String> errorPublisher() {
			return Flowable.error(new RuntimeException("error over publisher"));
		}

		/** query should return value */
		@GraphQLQuery
		@GraphQLMutation
		public CompletableFuture<String> valueCompletableFuture() {
			return CompletableFuture.supplyAsync(() -> "value over completable future");
		}

		/** query should fail with {@link GraphQLError} */
		@GraphQLQuery
		@GraphQLMutation
		public CompletableFuture<String> errorCompletableFuture() {
			return CompletableFuture.supplyAsync(() -> {
				throw new RuntimeException("error over completable future");
			});
		}
	}

	@BeforeEach
	private void buildGraphQL() {
		QueryService service = new QueryService();

		GraphQLSchema schema = new GraphQLSchemaGenerator() //
				.withBasePackages(QueryService.class.getPackage().getName()) //
				.withOperationsFromSingleton(service) //
				.generate();

		graphQL = GraphQL.newGraphQL(schema).build();
	}

	@Nested
	class ValuePublisher {

		@Test
		void executeQueryValuePublisher_successfulQueryWithPublisherResult_resultDataContainsArrayOfValue() {
			// arrange
			Object expectedData = Collections.singletonMap("valuePublisher", Arrays.asList("value over publisher"));

			// act
			ExecutionResult result = graphQL.execute("query{valuePublisher}");

			// assert data
			Object data = result.getData();
			assertEquals(expectedData, data);
		}

		@Test
		void executeQueryValuePublisher_successfulQueryWithPublisherResult_resultErrorsIsEmpty() {
			// arrange
			Object expectedErrors = Collections.emptyList();

			// act
			ExecutionResult result = graphQL.execute("query{valuePublisher}");

			// assert data
			List<GraphQLError> errors = result.getErrors();
			assertEquals(expectedErrors, errors);
		}

		@Test
		void executeMutationValuePublisher_successfulMutationWithPublisherResult_resultDataContainsArrayOfValue() {
			// arrange
			Object expectedData = Collections.singletonMap("valuePublisher", Arrays.asList("value over publisher"));

			// act
			ExecutionResult result = graphQL.execute("mutation{valuePublisher}");

			// assert data
			Object data = result.getData();
			assertEquals(expectedData, data);
		}

		@Test
		void executeMutationValuePublisher_successfulMutationWithPublisherResult_resultErrorsIsEmpty() {
			// arrange
			Object expectedErrors = Collections.emptyList();

			// act
			ExecutionResult result = graphQL.execute("mutation{valuePublisher}");

			// assert data
			List<GraphQLError> errors = result.getErrors();
			assertEquals(expectedErrors, errors);
		}
	}

	@Nested
	class ValueCompletableFuture {

		@Test
		void executeQueryValueCompletableFuture_successfulQueryWithFutureResult_resultDataContainsValue() {
			// arrange
			Object expectedData = Collections.singletonMap("valueCompletableFuture", "value over completable future");

			// act
			ExecutionResult result = graphQL.execute("query{valueCompletableFuture}");

			// assert data
			Object data = result.getData();
			assertEquals(expectedData, data);
		}

		@Test
		void executeQueryValuePublisher_successfulQueryWithFutureResult_resultErrorsIsEmpty() {
			// arrange
			Object expectedErrors = Collections.emptyList();

			// act
			ExecutionResult result = graphQL.execute("query{valueCompletableFuture}");

			// assert data
			List<GraphQLError> errors = result.getErrors();
			assertEquals(expectedErrors, errors);
		}

		@Test
		void executeMutationValueCompletableFuture_successfulMutationWithFutureResult_resultDataContainsValue() {
			// arrange
			Object expectedData = Collections.singletonMap("valueCompletableFuture", "value over completable future");

			// act
			ExecutionResult result = graphQL.execute("mutation{valueCompletableFuture}");

			// assert data
			Object data = result.getData();
			assertEquals(expectedData, data);
		}

		@Test
		void executeMutationValueCompletableFuture_successfulMutationWithFutureResult_resultErrorsIsEmpty() {
			// arrange
			Object expectedErrors = Collections.emptyList();

			// act
			ExecutionResult result = graphQL.execute("mutation{valuePublisher}");

			// assert data
			List<GraphQLError> errors = result.getErrors();
			assertEquals(expectedErrors, errors);
		}
	}

	@Nested
	class ErrorPublisher {

		@Test
		void executeQueryErrorPublisher_queryWithPublisherWithError_resultDataContainsEmptyArray() {
			SpqrVersion.assumeSupportsGraphQLVersion12();

			// arrange
			Object expectedData = Collections.singletonMap("errorPublisher", Collections.emptyList());

			// act
			ExecutionResult result = assertDoesNotThrow(() -> graphQL.execute("query{errorPublisher}"));

			// assert data
			Object data = result.getData();
			assertEquals(expectedData, data);
		}

		@Test
		void executeQueryErrorPublisher_queryWithPublisherWithError_resultErrorsContainsOneError() {
			SpqrVersion.assumeSupportsGraphQLVersion12();

			// act
			ExecutionResult result = assertDoesNotThrow(() -> graphQL.execute("query{errorPublisher}"));

			// assert data
			List<GraphQLError> errors = result.getErrors();
			assertNotNull(errors);
			assertEquals(1, errors.size());
		}

		@Test
		void executeMutationErrorPublisher_mutationWithPublisherWithError_resultDataContainsEmptyArray() {
			SpqrVersion.assumeSupportsGraphQLVersion12();

			// arrange
			Object expectedData = Collections.singletonMap("errorPublisher", Collections.emptyList());

			// act
			ExecutionResult result = assertDoesNotThrow(() -> graphQL.execute("mutation{errorPublisher}"));

			// assert data
			Object data = result.getData();
			assertEquals(expectedData, data);
		}

		@Test
		void executeMutationErrorPublisher_mutationWithPublisherWithError_resultErrorsContainsOneError() {
			SpqrVersion.assumeSupportsGraphQLVersion12();

			// act
			ExecutionResult result = assertDoesNotThrow(() -> graphQL.execute("mutation{errorPublisher}"));

			// assert data
			List<GraphQLError> errors = result.getErrors();
			assertNotNull(errors);
			assertEquals(1, errors.size());
		}

		@Test
		void executeSubscriptionErrorPublisher_subscriptionWithPublisherWithError_resultDataIsPublisherWithError() {
			// act
			ExecutionResult result = assertDoesNotThrow(() -> graphQL.execute("subscription{errorPublisher}"));

			// assert data
			Publisher<String> data = result.getData();
			assertNotNull(data);
			data.subscribe(new Subscriber<String>() {
				public void onSubscribe(Subscription s) {
					s.request(Long.MAX_VALUE);
				}

				@Override
				public void onError(Throwable t) {
					assertTrue(t instanceof RuntimeException);
				}

				@Override
				public void onNext(String next) {
					fail("Unexpected next value");
				}

				@Override
				public void onComplete() {
					fail("Unexpected complete");
				}
			});
		}

		@Test
		void executeSubscriptionErrorPublisher_queryWithPublisherWithError_resultErrorsRemainsEmpty() {
			// act
			ExecutionResult result = assertDoesNotThrow(() -> graphQL.execute("subscription{errorPublisher}"));

			// assert data
			List<GraphQLError> errors = result.getErrors();
			assertTrue(errors == null || errors.isEmpty());
		}
	}

	@Nested
	class ErrorCompletableFuture {
		@Test
		void executeQueryErrorCompletableFuture_queryWithCompletableFutureWithError_resultDataContainsNullResult() {
			// arrange
			Object expectedData = Collections.singletonMap("errorCompletableFuture", null);

			// act
			ExecutionResult result = assertDoesNotThrow(() -> graphQL.execute("query{errorCompletableFuture}"));

			// assert data
			Object data = result.getData();
			assertEquals(expectedData, data);
		}

		@Test
		void executeQueryErrorCompletableFuture_queryWithCompletableFutureWithError_resultErrorsContainsOneError() {
			// act
			ExecutionResult result = assertDoesNotThrow(() -> graphQL.execute("query{errorCompletableFuture}"));

			// assert data
			List<GraphQLError> errors = result.getErrors();
			assertNotNull(errors);
			assertEquals(1, errors.size());
		}

		@Test
		void executeMutationErrorCompletableFuture_mutationWithCompletableFutureWithError_resultDataContainsNullResult() {
			// arrange
			Object expectedData = Collections.singletonMap("errorCompletableFuture", null);

			// act
			ExecutionResult result = assertDoesNotThrow(() -> graphQL.execute("mutation{errorCompletableFuture}"));

			// assert data
			Object data = result.getData();
			assertEquals(expectedData, data);
		}

		@Test
		void executeMutationErrorCompletableFuture_mutationWithCompletableFutureWithError_resultErrorsContainsOneError() {
			// act
			ExecutionResult result = assertDoesNotThrow(() -> graphQL.execute("mutation{errorCompletableFuture}"));

			// assert data
			List<GraphQLError> errors = result.getErrors();
			assertNotNull(errors);
			assertEquals(1, errors.size());
		}
	}

}
