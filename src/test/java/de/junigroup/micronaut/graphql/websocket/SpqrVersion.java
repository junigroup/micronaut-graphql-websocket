/*
 * Copyright 2019 original author
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.junigroup.micronaut.graphql.websocket;

import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import org.opentest4j.AssertionFailedError;

import io.leangen.graphql.annotations.GraphQLQuery;
import io.micronaut.core.version.SemanticVersion;

/** Helper to estimate and check referenced SPQR version */
class SpqrVersion {
	/**
	 * SPQR 0.9.9 officially supports GraphQL-Java version 11. <br />
	 * But GraphQL-Java version 12 is default in micronaut".
	 */
	static void assumeSupportsGraphQLVersion12() {
		String estimateSqprVersion = estimateSqprVersion();
		assumeTrue(SemanticVersion.isAtLeastMajorMinor(estimateSqprVersion, 1, 0),
				"\n  Sqpr at least version 1.0 to support GraphQL version 12.0.\n  Current version is " + estimateSqprVersion);
	}

	private static String estimateSqprVersion() {
		try {
			InputStream propertiesAsStream = findResourceAsStream(GraphQLQuery.class, "/META-INF/maven/io.leangen.graphql/spqr/pom.properties");
			Properties properties = new Properties();
			properties.load(propertiesAsStream);
			propertiesAsStream.close();
			String version = properties.getProperty("version");
			return version;
		} catch (IOException e) {
			throw new AssertionFailedError("GraphQL version unspecified: " + e.getMessage());
		}
	}

	private static InputStream findResourceAsStream(Class<?> referenceClass, String rootRelativePath) throws MalformedURLException, IOException {
		URL classUrl = referenceClass.getResource(referenceClass.getSimpleName() + ".class");
		String toRootPath = referenceClass.getName().replace('.', '/').replaceAll("\\w+", "..");
		URL resourceUrl = new URL(classUrl, toRootPath + rootRelativePath);
		InputStream resourceAsStream = resourceUrl.openStream();
		return resourceAsStream;
	}
}
