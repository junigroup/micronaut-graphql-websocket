/*
 * Copyright 2019 original author
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.junigroup.micronaut.graphql.websocket;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import org.reactivestreams.Publisher;

import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.annotations.GraphQLSubscription;
import io.reactivex.Flowable;

/**
 * A controller supplying {@link GraphQLSubscription} methods with different subscription handling.
 * 
 * @author Lars Niedziolka
 */
public class GraphQLTestController {
	/*** Subscription streamed data */
	public static class SerialData {
		public int index;

		public SerialData(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}
	}

	@GraphQLQuery
	@GraphQLMutation
	@GraphQLSubscription
	public Publisher<SerialData> serialEmpty() {
		return Flowable.empty();
	}

	@GraphQLQuery
	@GraphQLMutation
	public SerialData synchronRequest() {
		return new SerialData(1);
	}

	@GraphQLQuery
	@GraphQLMutation
	public CompletableFuture<SerialData> asyncCompletableFuture() {
		return CompletableFuture.completedFuture(new SerialData(1));
	}

	@GraphQLQuery
	@GraphQLMutation
	@GraphQLSubscription
	public Publisher<SerialData> serialSingle() {
		return Flowable.just(new SerialData(1));
	}

	@GraphQLQuery
	@GraphQLMutation
	@GraphQLSubscription
	public Publisher<SerialData> serialError() {
		return Flowable.error(new RuntimeException("Error in serialError"));
	}

	@GraphQLQuery
	@GraphQLMutation
	@GraphQLSubscription
	public Publisher<SerialData> serialExecutionException() {
		throw new RuntimeException("Exception in serialExecutionException");
	}

	@GraphQLQuery
	@GraphQLMutation
	@GraphQLSubscription
	public Publisher<SerialData> serialData5() {
		return Flowable.range(1, 5).map(SerialData::new);
	}

	@GraphQLQuery
	@GraphQLMutation
	@GraphQLSubscription
	public Publisher<SerialData> serialData5Error() {
		return Flowable.concat(Flowable.range(1, 5).map(SerialData::new), Flowable.error(new RuntimeException("Error in serialData5Error")));
	}

	@GraphQLQuery
	@GraphQLMutation
	@GraphQLSubscription
	public Publisher<SerialData> serialDataDelayData() {
		return Flowable.concat(Flowable.just(new SerialData(1)), Flowable.just(new SerialData(2)).delay(100, TimeUnit.MILLISECONDS));
	}
}
