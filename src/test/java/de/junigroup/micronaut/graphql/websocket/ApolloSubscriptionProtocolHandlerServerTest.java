/*
 * Copyright 2019 original author
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.junigroup.micronaut.graphql.websocket;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import org.junit.jupiter.api.Test;

import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.MutableHttpRequest;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.runtime.server.EmbeddedServer;
import io.micronaut.test.annotation.MicronautTest;
import io.micronaut.test.annotation.MockBean;
import io.micronaut.websocket.RxWebSocketClient;

/**
 * Tests the {@link ApolloSubscriptionProtocolHandler} by request an {@link EmbeddedServer} over WebSocket.
 * 
 * @author Lars Niedziolka
 */
@MicronautTest
public class ApolloSubscriptionProtocolHandlerServerTest {
	/*
	 * Other as for ApolloSubscriptionProtocolHandlerDirectTest we avoid @Nested test classes. I could not get micronaut to
	 * use @Nested and @Inject and beans handling at the same time.
	 */

	@Client("/")
	@Inject
	RxWebSocketClient websocketClient;

	@MockBean(ApolloKeepAliveTicker.class)
	ApolloKeepAliveHandler keepAliveHandler() {
		return new ApolloKeepAliveStub();
	}

	@Test
	void testNoSubprotocol_protocolError_connectedRejected() throws InterruptedException {
		MutableHttpRequest<Object> httpRequest = HttpRequest.GET("/subscriptions");

		RecordingWebSocket webSocket = websocketClient.connect(RecordingWebSocket.class, httpRequest).blockingFirst();

		webSocket.tester.awaitDone(1, TimeUnit.SECONDS);
		webSocket.tester.assertComplete();
		webSocket.tester.assertValues( //
				"open", //
				"close: CloseReason{code=1002, reason='Protocol Error'}" //
		);
	}

	@Test
	void testUnsupportedSubprotocol_protocolError_connectedRejected() throws InterruptedException {
		MutableHttpRequest<Object> httpRequest = HttpRequest.GET("/subscriptions");
		httpRequest.getHeaders().add(HttpHeaders.SEC_WEBSOCKET_PROTOCOL, "unsupported-subprotocol");

		RecordingWebSocket webSocket = websocketClient.connect(RecordingWebSocket.class, httpRequest).blockingFirst();

		webSocket.tester.awaitDone(1, TimeUnit.SECONDS);
		webSocket.tester.assertComplete();
		webSocket.tester.assertValues( //
				"open", //
				"close: CloseReason{code=1002, reason='Protocol Error'}" //
		);
	}

	@Test
	void testQueryOverWebsocket_connectionInit_receiveConnectionAck() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		try {
			webSocket.sendSync(message("{'type':'connection_init'}"));
		} catch (Throwable e) {
			e.printStackTrace();
		}
		webSocket.tester.awaitCount(4);
		webSocket.tester.assertNoErrors();
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}" //
		);
	}

	@Test
	void testSubscriptionOverWebsocket_subscribeEmpty_receiveComplete() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'subscription{ serialEmpty{index} }'}}"));

		webSocket.tester.awaitCount(6);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'subscription{ serialEmpty{index} }'}}", //
				"message < {'type':'complete','id':'1'}" //
		);
	}

	@Test
	void testSubscriptionOverWebsocket_subscribeSingle_receiveDataComplete() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'subscription{ serialSingle{index} }'}}"));

		webSocket.tester.awaitCount(7);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'subscription{ serialSingle{index} }'}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialSingle':{'index':1}}}}", //
				"message < {'type':'complete','id':'1'}" //
		);
	}

	@Test
	void testSubscriptionOverWebsocket_subscribeData5_receiveData5xComplete() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'subscription{ serialData5{index} }'}}"));

		webSocket.tester.awaitCount(11);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'subscription{ serialData5{index} }'}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialData5':{'index':1}}}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialData5':{'index':2}}}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialData5':{'index':3}}}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialData5':{'index':4}}}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialData5':{'index':5}}}}", //
				"message < {'type':'complete','id':'1'}" //
		);
	}

	@Test
	void testSubscriptionOverWebsocket_subscribeError_receiveError() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'subscription{ serialError{index} }'}}"));

		String error = "{'message':'Error in serialError'}";
		webSocket.tester.awaitCount(6);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'subscription{ serialError{index} }'}}", //
				"message < {'type':'error','id':'1','payload':" + error + "}" //
		);
	}

	@Test
	void testSubscriptionOverWebsocket_subscribeData5Error_receiveData5xError() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'subscription{ serialData5Error{index} }'}}"));

		String error = "{'message':'Error in serialData5Error'}";
		webSocket.tester.awaitCount(11);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'subscription{ serialData5Error{index} }'}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialData5Error':{'index':1}}}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialData5Error':{'index':2}}}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialData5Error':{'index':3}}}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialData5Error':{'index':4}}}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialData5Error':{'index':5}}}}", //
				"message < {'type':'error','id':'1','payload':" + error + "}" //
		);
	}

	@Test
	void testSubscriptionOverWebsocket_subscribeExecutionException_receiveDataWithErrorAndNullData() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'subscription{ serialExecutionException{index} }'}}"));

		String error = "{'message':'Exception while fetching data (/serialExecutionException) : Exception in serialExecutionException','locations':[{'line':1,'column':15}],'path':['serialExecutionException']}";
		webSocket.tester.awaitCount(6);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'subscription{ serialExecutionException{index} }'}}", //
				"message < {'type':'data','id':'1','payload':{'errors':[" + error + "],'data':null}}", //
				"message < {'type':'complete','id':'1'}" //
		);
	}

	@Test
	void testSubscriptionOverWebsocket_stopSerialSingleAfterComplete_receiveDataComplete() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'subscription{ serialSingle{index} }'}}"));
		webSocket.sendSync(message("{'id':'1','type':'stop'}"));

		webSocket.tester.awaitCount(8);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'subscription{ serialSingle{index} }'}}", //
				"message > {'id':'1','type':'stop'}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialSingle':{'index':1}}}}", //
				"message < {'type':'complete','id':'1'}" //
		);
	}

	@Test
	void testSubscriptionOverWebsocket_stopSerialDataDelayData_receiveFirstDataStop() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'subscription{ serialDataDelayData{index} }'}}"));
		webSocket.sendSync(message("{'id':'1','type':'stop'}"));

		webSocket.tester.awaitCount(7);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'subscription{ serialDataDelayData{index} }'}}", //
				"message > {'id':'1','type':'stop'}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialDataDelayData':{'index':1}}}}" //
		);
	}

	@Test
	void testSubscriptionOverWebsocket_connectionTerminateSerialSingleAfterComplete_receiveDataCompleteNormalClose() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'subscription{ serialSingle{index} }'}}"));
		webSocket.sendSync(message("{'type':'connection_terminate'}"));

		webSocket.tester.awaitDone(1, TimeUnit.SECONDS);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'subscription{ serialSingle{index} }'}}", //
				"message > {'type':'connection_terminate'}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialSingle':{'index':1}}}}", //
				"message < {'type':'complete','id':'1'}", //
				"close: CloseReason{code=1000, reason='Normal Closure'}" //
		);
	}

	@Test
	void testSubscriptionOverWebsocket_connectionTerminateSerialDataDelayData_receiveFirstDataNormalClose() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'subscription{ serialDataDelayData{index} }'}}"));
		webSocket.sendSync(message("{'type':'connection_terminate'}"));

		webSocket.tester.awaitDone(1, TimeUnit.SECONDS);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'subscription{ serialDataDelayData{index} }'}}", //
				"message > {'type':'connection_terminate'}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialDataDelayData':{'index':1}}}}", //
				"close: CloseReason{code=1000, reason='Normal Closure'}" //
		);
	}

	@Test
	void testQueryOverWebsocket_queryEmpty_receiveEmptyArrayComplete() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'query{ serialEmpty{index} }'}}"));

		webSocket.tester.awaitCount(7);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'query{ serialEmpty{index} }'}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialEmpty':[]}}}", //
				"message < {'type':'complete','id':'1'}" //
		);
	}

	@Test
	void testQueryOverWebsocket_synchronRequest_receiveSingleDataAsScalarComplete() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'query{ synchronRequest{index} }'}}"));

		webSocket.tester.awaitCount(7);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'query{ synchronRequest{index} }'}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'synchronRequest':{'index':1}}}}", //
				"message < {'type':'complete','id':'1'}" //
		);
	}

	@Test
	void testQueryOverWebsocket_asyncCompletableFuture_receiveSingleDataAsScalarComplete() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'query{ asyncCompletableFuture{index} }'}}"));

		webSocket.tester.awaitCount(7);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'query{ asyncCompletableFuture{index} }'}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'asyncCompletableFuture':{'index':1}}}}", //
				"message < {'type':'complete','id':'1'}" //
		);
	}

	@Test
	void testQueryOverWebsocket_querySingle_receiveArrayOfSingleDataComplete() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'query{ serialSingle{index} }'}}"));

		webSocket.tester.awaitCount(7);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'query{ serialSingle{index} }'}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialSingle':[{'index':1}]}}}", //
				"message < {'type':'complete','id':'1'}" //
		);
	}

	@Test
	void testQueryOverWebsocket_queryData5_receiveArrayOf5DataComplete() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'query{ serialData5{index} }'}}"));

		webSocket.tester.awaitCount(7);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'query{ serialData5{index} }'}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialData5':[{'index':1},{'index':2},{'index':3},{'index':4},{'index':5}]}}}", //
				"message < {'type':'complete','id':'1'}" //
		);
	}

	@Test
	void testQueryOverWebsocket_queryError_receiveError() {
		SpqrVersion.assumeSupportsGraphQLVersion12();

		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'query{ serialError{index} }'}}"));

		String error = "{'message':'Error in serialError','locations':[]}";
		webSocket.tester.awaitCount(6);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'query{ serialError{index} }'}}", //
				"message < {'type':'error','id':'1','payload':" + error + "}" //
		);
	}

	@Test
	void testQueryOverWebsocket_queryData5Error_receiveError() {
		SpqrVersion.assumeSupportsGraphQLVersion12();

		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'query{ serialData5Error{index} }'}}"));

		String error = "{'message':'Error in serialData5Error','locations':[]}";
		webSocket.tester.awaitCount(6);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'query{ serialData5Error{index} }'}}", //
				"message < {'type':'error','id':'1','payload':" + error + "}" //
		);
	}

	@Test
	void testQueryOverWebsocket_queryExecutionException_receiveDataWithErrorAndNullResultData() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'query{ serialExecutionException{index} }'}}"));

		String error = "{'message':'Exception while fetching data (/serialExecutionException) : Exception in serialExecutionException','locations':[{'line':1,'column':8}],'path':['serialExecutionException']}";
		webSocket.tester.awaitCount(7);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'query{ serialExecutionException{index} }'}}", //
				"message < {'type':'data','id':'1','payload':{'errors':[" + error + "],'data':{'serialExecutionException':null}}}", //
				"message < {'type':'complete','id':'1'}" //
		);
	}

	@Test
	void testQueryOverWebsocket_stopSerialSingleAfterComplete_receiveArrayOfSingleDataComplete() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'query{ serialSingle{index} }'}}"));
		webSocket.sendSync(message("{'id':'1','type':'stop'}"));

		webSocket.tester.awaitCount(8);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'query{ serialSingle{index} }'}}", //
				"message > {'id':'1','type':'stop'}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialSingle':[{'index':1}]}}}", //
				"message < {'type':'complete','id':'1'}" //
		);
	}

	@Test
	void testQueryOverWebsocket_stopSerialDataDelayData_receiveNoData() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'query{ serialDataDelayData{index} }'}}"));
		webSocket.sendSync(message("{'id':'1','type':'stop'}"));

		webSocket.tester.awaitCount(4);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'query{ serialDataDelayData{index} }'}}", //
				"message > {'id':'1','type':'stop'}" //
		);
	}

	@Test
	void testQueryOverWebsocket_connectionTerminateSerialSingleAfterComplete_receiveArrayOfSingleDataCompleteNormalClose() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'query{ serialSingle{index} }'}}"));
		webSocket.sendSync(message("{'type':'connection_terminate'}"));

		webSocket.tester.awaitDone(1, TimeUnit.SECONDS);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'query{ serialSingle{index} }'}}", //
				"message > {'type':'connection_terminate'}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialSingle':[{'index':1}]}}}", //
				"message < {'type':'complete','id':'1'}", //
				"close: CloseReason{code=1000, reason='Normal Closure'}" //
		);
	}

	@Test
	void testQueryOverWebsocket_connectionTerminateSerialDataBeforeComplete_receiveNoDataNormalClose() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'query{ serialDataDelayData{index} }'}}"));
		webSocket.sendSync(message("{'type':'connection_terminate'}"));

		webSocket.tester.awaitDone(1, TimeUnit.SECONDS);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'query{ serialDataDelayData{index} }'}}", //
				"message > {'type':'connection_terminate'}", //
				"close: CloseReason{code=1000, reason='Normal Closure'}" //
		);
	}

	@Test
	void testMutationOverWebsocket_mutateWithEmptyResult_receiveEmptyArrayComplete() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'mutation{ serialEmpty{index} }'}}"));

		webSocket.tester.awaitCount(7);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'mutation{ serialEmpty{index} }'}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialEmpty':[]}}}", //
				"message < {'type':'complete','id':'1'}" //
		);
	}

	@Test
	void testMutationOverWebsocket_synchronMutation_receiveSingleDataAsScalarComplete() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'mutation{ synchronRequest{index} }'}}"));

		webSocket.tester.awaitCount(7);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'mutation{ synchronRequest{index} }'}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'synchronRequest':{'index':1}}}}", //
				"message < {'type':'complete','id':'1'}" //
		);
	}

	@Test
	void testMutationOverWebsocket_mutationWithAsyncCompletableFuture_receiveSingleDataAsScalarComplete() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'mutation{ asyncCompletableFuture{index} }'}}"));

		webSocket.tester.awaitCount(7);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'mutation{ asyncCompletableFuture{index} }'}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'asyncCompletableFuture':{'index':1}}}}", //
				"message < {'type':'complete','id':'1'}" //
		);
	}

	@Test
	void testMutationOverWebsocket_mutationWithSingleResult_receiveArrayOfSingleDataComplete() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'mutation{ serialSingle{index} }'}}"));

		webSocket.tester.awaitCount(7);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'mutation{ serialSingle{index} }'}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialSingle':[{'index':1}]}}}", //
				"message < {'type':'complete','id':'1'}" //
		);
	}

	@Test
	void testMutationOverWebsocket_mutationWith5Results_receiveArrayOf5DataComplete() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'mutation{ serialData5{index} }'}}"));

		webSocket.tester.awaitCount(7);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'mutation{ serialData5{index} }'}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialData5':[{'index':1},{'index':2},{'index':3},{'index':4},{'index':5}]}}}", //
				"message < {'type':'complete','id':'1'}" //
		);
	}

	@Test
	void testMutationOverWebsocket_mutationWithPublisherError_receiveError() {
		SpqrVersion.assumeSupportsGraphQLVersion12();

		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'mutation{ serialError{index} }'}}"));

		String error = "{'message':'Error in serialError','locations':[]}";
		webSocket.tester.awaitDone(1, TimeUnit.SECONDS);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'mutation{ serialError{index} }'}}", //
				"message < {'type':'error','id':'1','payload':" + error + "}" //
		);
	}

	@Test
	void testMutationOverWebsocket_mutationWithPublisher5ResultsAndError_receiveError() {
		SpqrVersion.assumeSupportsGraphQLVersion12();

		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'mutation{ serialData5Error{index} }'}}"));

		String error = "{'message':'Error in serialData5Error','locations':[]}";
		webSocket.tester.awaitDone(1, TimeUnit.SECONDS);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > ", //
				"message < {'type':'error','id':'1','payload':" + error + "}" //
		);
	}

	@Test
	void testMutationOverWebsocket_mutationExecutionException_receiveDataWithErrorAndNullResultData() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'mutation{ serialExecutionException{index} }'}}"));

		String error = "{'message':'Exception while fetching data (/serialExecutionException) : Exception in serialExecutionException','locations':[{'line':1,'column':11}],'path':['serialExecutionException']}";
		webSocket.tester.awaitCount(7);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'mutation{ serialExecutionException{index} }'}}", //
				"message < {'type':'data','id':'1','payload':{'errors':[" + error + "],'data':{'serialExecutionException':null}}}", //
				"message < {'type':'complete','id':'1'}" //
		);
	}

	@Test
	void testMutationOverWebsocket_stopSerialSingleAfterComplete_receiveArrayOfSingleDataComplete() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'mutation{ serialSingle{index} }'}}"));
		webSocket.tester.awaitCount(7);
		webSocket.sendSync(message("{'id':'1','type':'stop'}"));

		webSocket.tester.awaitCount(8);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'mutation{ serialSingle{index} }'}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialSingle':[{'index':1}]}}}", //
				"message < {'type':'complete','id':'1'}", //
				"message > {'id':'1','type':'stop'}" //
		);
	}

	@Test
	void testMutationOverWebsocket_stopSerialDataDelayData_receiveNoData() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'mutation{ serialDataDelayData{index} }'}}"));
		webSocket.sendSync(message("{'id':'1','type':'stop'}"));

		webSocket.tester.awaitDone(1, TimeUnit.SECONDS);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'mutation{ serialDataDelayData{index} }'}}", //
				"message > {'id':'1','type':'stop'}" //
		);
	}

	@Test
	void testMutationOverWebsocket_connectionTerminateSerialSingleAfterComplete_receiveArrayOfSingleDataCompleteNormalClose() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'mutation{ serialSingle{index} }'}}"));
		webSocket.tester.awaitCount(7);
		webSocket.sendSync(message("{'type':'connection_terminate'}"));

		webSocket.tester.awaitDone(1, TimeUnit.SECONDS);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'mutation{ serialSingle{index} }'}}", //
				"message < {'type':'data','id':'1','payload':{'data':{'serialSingle':[{'index':1}]}}}", //
				"message < {'type':'complete','id':'1'}", //
				"message > {'type':'connection_terminate'}", //
				"close: CloseReason{code=1000, reason='Normal Closure'}" //
		);
	}

	@Test
	void testMutationOverWebsocket_connectionTerminateSerialDataBeforeComplete_receiveNoDataNormalClose() {
		RecordingWebSocket webSocket = startGraphQLWebSocketRequest(websocketClient);
		webSocket.sendSync(message("{'type':'connection_init'}"));
		webSocket.tester.awaitCount(4);

		webSocket.sendSync(message("{'id':'1','type':'start','payload':{'query':'mutation{ serialDataDelayData{index} }'}}"));
		webSocket.sendSync(message("{'type':'connection_terminate'}"));

		webSocket.tester.awaitDone(1, TimeUnit.SECONDS);
		webSocket.tester.assertValues( //
				"open", //
				"message > {'type':'connection_init'}", //
				"message < {'type':'connection_ack'}", //
				"message < {'type':'ka'}", //
				"message > {'id':'1','type':'start','payload':{'query':'mutation{ serialDataDelayData{index} }'}}", //
				"message > {'type':'connection_terminate'}", //
				"close: CloseReason{code=1000, reason='Normal Closure'}" //
		);
	}

	private static RecordingWebSocket startGraphQLWebSocketRequest(RxWebSocketClient websocketClient) {
		MutableHttpRequest<Object> httpRequest = HttpRequest.GET("/subscriptions");
		httpRequest.getHeaders().add(HttpHeaders.SEC_WEBSOCKET_PROTOCOL, "graphql-ws");
		RecordingWebSocket recorder = websocketClient.connect(RecordingWebSocket.class, httpRequest).blockingFirst();
		return Objects.requireNonNull(recorder, "RecordingWebSocket");
	}

	private static String message(String string) {
		return string.replace('\'', '"');
	}

}
