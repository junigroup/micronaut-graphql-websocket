# Micronaut GraphQL WebSocket #

This project extends [Micronaut GraphQL](https://github.com/micronaut-projects/micronaut-graphql) 
with support for subscriptions over websocket, following the [Apollo Protocol](https://github.com/apollographql/subscriptions-transport-ws/blob/master/PROTOCOL.md).

The Micronaut GraphQL WebSocket integration can be used together with other GraphQL integration libraries like
[GraphQL Java Tools](https://github.com/graphql-java-kickstart/graphql-java-tools) and [GraphQL SPQR](https://github.com/leangen/graphql-spqr).

## Documentation ##

... will come later.

## Examples ##

... will come later.
